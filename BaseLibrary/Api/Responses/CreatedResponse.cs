﻿using Microsoft.AspNetCore.Http;

namespace BaseLibrary.Api.Responses
{
    public class CreatedResponse : ApiResponse
    {
        public int Id { get; set; }

        public CreatedResponse(int id, string message = null, object result = null) : base(StatusCodes.Status201Created, message, result)
        {
            Id = id;
        }
    }

    //public class CreatedResponse<T> : ApiResponse
    //{
    //    public T Id { get; set; }

    //    public CreatedResponse(T id, string message = null, object result = null) : base(StatusCodes.Status201Created, message, result)
    //    {
    //        Id = id;
    //    }
    //}
}
