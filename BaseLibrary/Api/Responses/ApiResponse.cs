﻿using Microsoft.AspNetCore.Http;

namespace BaseLibrary
{
    public class ApiResponse
    {
        public int StatusCode { get; }
        public string Message { get; }
        public object Result { get; }

        public ApiResponse(int statusCode, string message = null, object result = null)
        {
            StatusCode = statusCode;
            Message = message ?? GetDefaultMessageForStatusCode(statusCode);
            Result = result;
        }

        string GetDefaultMessageForStatusCode(int statusCode)
        {
            switch (statusCode)
            {
                case StatusCodes.Status200OK:
                    return "Success";
                case StatusCodes.Status201Created:
                    return "Created";
                case StatusCodes.Status400BadRequest:
                    return "Bad Request";
                case StatusCodes.Status401Unauthorized:
                    return "Unauthorized";
                case StatusCodes.Status404NotFound:
                    return "Resource not found";
                case StatusCodes.Status500InternalServerError:
                    return "There was an error processing your request";
                default:
                    return null;
            }
        }
    }
}
