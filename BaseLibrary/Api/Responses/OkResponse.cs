﻿using Microsoft.AspNetCore.Http;

namespace BaseLibrary.Api.Responses
{
    public class OkResponse : ApiResponse
    {
        public OkResponse(string message = null, object result = null) : base(StatusCodes.Status200OK, message, result) { }
    }
}
