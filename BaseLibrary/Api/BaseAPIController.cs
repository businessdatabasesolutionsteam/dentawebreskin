﻿using BaseLibrary.Api.Responses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace BaseLibrary
{
    public class BaseAPIController : Controller
    {
        #region Success / Ok
        protected IActionResult Success()
        {
            return Ok(new OkResponse());
        }

        protected IActionResult Success(string message)
        {
            return Ok(new OkResponse(message));
        }

        protected IActionResult Success(object result)
        {
            return Ok(new OkResponse(result: result));
        }

        protected IActionResult Success(string message, object result)
        {
            return Ok(new OkResponse(message, result));
        }
        #endregion Success / Ok

        //#region Created At Route 
        //protected IActionResult CreatedAtRoute(string routeName, int createdId)
        //{
        //    return CreatedAtRoute(routeName, new { id = createdId }, new CreatedResponse(createdId));
        //}

        //protected IActionResult CreatedAtRoute(string routeName, string message, int createdId)
        //{
        //    return CreatedAtRoute(routeName, new { id = createdId }, new CreatedResponse(createdId, message));
        //}

        //protected IActionResult CreatedAtRoute(string routeName, string message, object result, int createdId)
        //{
        //    return CreatedAtRoute(routeName, new { id = createdId }, new CreatedResponse(createdId, message, result));
        //}
        //#endregion Created At Route    

        #region Error
        protected IActionResult Error()
        {
            var result = new ObjectResult(new ApiResponse(StatusCodes.Status500InternalServerError))
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };
            return result;
        }

        protected IActionResult Error(int code)
        {
            var result = new ObjectResult(new ApiResponse(code))
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };
            return result;
        }

        protected IActionResult Error(string message)
        {
            var result = new ObjectResult(new ApiResponse(StatusCodes.Status500InternalServerError, message))
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };
            return result;
        }

        protected IActionResult Error(int code, string message)
        {
            var result = new ObjectResult(new ApiResponse(code, message))
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };
            return result;
        }
        #endregion Error

        #region Authorization
        protected IActionResult NotAuthorized(string message)
        {
            return BadRequest(new ApiResponse(StatusCodes.Status401Unauthorized, message));
        }
        #endregion Authorization
    }
}
