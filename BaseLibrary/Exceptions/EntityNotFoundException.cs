﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseLibrary.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public int EntityId { get; set; }

        public EntityNotFoundException(string message) : base(message)
        {
        }

        public EntityNotFoundException(string message, int entityId) : base(message)
        {
            this.EntityId = entityId;
        }
    }
}
