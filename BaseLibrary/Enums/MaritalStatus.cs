﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseLibrary.Enums
{
    public enum MaritalStatus
    {
        Married = 1,
        Single = 2,
        Widowed = 3,
    }
}
