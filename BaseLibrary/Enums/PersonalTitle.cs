﻿namespace BaseLibrary.Enums
{
    public enum PersonalTitle
    {
        Mr = 1,
        Mrs = 2,
        Ms = 3,
        Dr = 4
    }
}
