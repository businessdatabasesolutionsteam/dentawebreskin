﻿namespace BaseLibrary.Enums
{
    public enum Ethnicity
    {
        Caucasian = 1,
        Hispanic = 2,
        AfricanAmerican = 3,
        AmericanIndian = 4,
        Asian = 5,
        PacificIslander = 6,
        Other = 99
    }
}
