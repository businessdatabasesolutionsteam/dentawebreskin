﻿using System;

namespace BaseLibrary.Interfaces
{
    public interface IPersistedEntity
    {
        int Id { get; set; }
        string CreatedBy { get; set; }
        DateTime CreatedOn { get; set; }
        string UpdatedBy { get; set; }
        DateTime? UpdatedOn { get; set; }
    }
}
