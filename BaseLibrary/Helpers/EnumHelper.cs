﻿using BaseLibrary.Enums;
using BaseLibrary.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseLibrary.Helpers
{
    public static class EnumHelper
    {
        public static IEnumerable<EnumInformation> Enumerate<TEnum>() where TEnum : struct, IConvertible //Enum c# 7.3
        {
            foreach (object e in Enum.GetValues(typeof(TEnum)))
            {
                yield return new EnumInformation()
                {
                    Id = (int)e,
                    Value = e.ToString(),
                    Description = e.ToString().SplitCamelCase()
                };
            }
        }
    }

    public class EnumInformation
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
