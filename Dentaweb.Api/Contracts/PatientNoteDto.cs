﻿using Dentaweb.Data.Patients;

namespace Dentaweb.Api.Contracts
{
    public class PatientNoteDto : IPatientNote
    {
        public int PatientId { get; set; }
        public PatientNoteType NoteType { get; set; } // todo or string?
        public string NoteText { get; set; }
    }
}

