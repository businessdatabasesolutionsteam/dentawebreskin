﻿using Dentaweb.Data.Patients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dentaweb.Api.Contracts
{
    public class AppointmentDto : IAppointment
    {
        public int PatientId { get; set; }
        public int OfficeId { get; set; }
        public DateTime AppointmentDateTime { get; set; }
        public string AppointmentStatus { get; set; }
    }
}
