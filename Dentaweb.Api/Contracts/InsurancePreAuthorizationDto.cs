﻿using Dentaweb.Data.Insurance;
using System;

namespace Dentaweb.Api.Contracts
{
    public class InsurancePreAuthorizationDto : IInsurancePreAuthorization
    {
        public int PatientId { get; set; }
        public int OfficeId { get; set; }
        public string InsuranceId { get; set; }
        public DateTime PreAuthDate { get; set; }
        public int AppointmentId { get; set; }
        public int ProviderId { get; set; }
        public int ProductCode { get; set; }
        public PreAuthorizationType PreAuthorizationStatus { get; set; }
        public string PreAuthConfirmationNumber { get; set; }
    }
}
