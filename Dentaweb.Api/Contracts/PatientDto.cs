﻿using BaseLibrary.Enums;
using Dentaweb.Data.Patients;
using System;

namespace Dentaweb.Api.Contracts
{
    public class PatientDto :IPatient
    {
        public string PatientName { get; set; }
        public string Telephone { get; set; }
        public string EmailAddress { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string ServiceType { get; set; }
        public PersonalTitle Title { get; set; }
        public string Salutation { get; set; }
        public Gender Gender { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public string DriverLicenseNo { get; set; }
        public string SocialSecurityNo { get; set; }
        public Ethnicity Ethnicity { get; set; }
        public Ethnicity Race { get; set; }
        public ReferralSource ReferralSource { get; set; }
        public string PreviousDentist { get; set; }
        public string PreviousDentistPhone { get; set; }
        public StudentStatus StudentStatus { get; set; }
        public bool HasDentalInsurance { get; set; }
        public bool HasMedicalInsurance { get; set; }
        public string PreviousProvider { get; set; }
        public string PrevProviderPhone { get; set; }
        public string MobilePhone { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string EmergencyContact { get; set; }
        public string RelationshipToPatient { get; set; }
        public string EmergencyContactPhone { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
