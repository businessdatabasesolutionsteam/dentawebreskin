﻿using BaseLibrary;
using BaseLibrary.Enums;
using BaseLibrary.Exceptions;
using BaseLibrary.Helpers;
using Dentaweb.Api.Contracts;
using Dentaweb.Data.Enums;
using Dentaweb.Data.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dentaweb.Api.Controllers
{
    [Route("api/submission")]
    [ApiController]
    public class SubmissionController : BaseAPIController
    {
        private readonly ISubmissionRepository _repo;
        private readonly ILogger<SubmissionController> _log;

        public SubmissionController(ISubmissionRepository repo, ILogger<SubmissionController> log)
        {
            _repo = repo;
            _log = log;
        }

        /// <summary>
        ///     Get submission health history for a patient
        /// </summary>
        /// <param name="patientid">patient Id</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("healthhistory/{patientid}", Name = "GetSubmissionHealthHistory")]
        public IActionResult GetSubmissionHealthHistory(int patientid)
        {
            return Success(_repo.GetEntireHealthHistory(patientid));
        }

        /// <summary>
        ///     Get submission tooth charting for a patient
        /// </summary>
        /// <param name="patientid">patient Id</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("toothcharting/{patientid}", Name = "GetPatientToothCharting")]
        public IActionResult GetPatientToothCharting(int patientid)
        {
            return Success(_repo.GetPatientToothCharting(patientid));
        }

        /// <summary>
        ///     Get submission arch analysis for a patient
        /// </summary>
        /// <param name="patientid">patient Id</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("archanalysis/{patientid}", Name = "GetPatientArchAnalysis")]
        public IActionResult GetPatientArchAnalysis(int patientid)
        {
            return Success(_repo.GetPatientArchAnalysis(patientid));
        }

        /// <summary>
        ///     Get submission objectives for a patient
        /// </summary>
        /// <param name="patientid">patient Id</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("patientobjective/{patientid}", Name = "GetPatientObjectives")]
        public IActionResult GetPatientObjectives(int patientid)
        {
            return Success(_repo.GetPatientObjectives(patientid));
        }

        /// <summary>
        ///     Get submission misc docs for a patient
        /// </summary>
        /// <param name="patientid">patient Id</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("miscdocs/{patientid}", Name = "GetPatientMiscDocs")]
        public IActionResult GetPatientMiscDocs(int patientid)
        {
            return Success(_repo.GetPatientMiscDocs(patientid));
        }

        /// <summary>
        ///     Get submission order details for a patient
        /// </summary>
        /// <param name="patientid">patient Id</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("orderdetails/{patientid}", Name = "GetPatientOrderDetails")]
        public IActionResult GetPatientOrderDetails(int patientid)
        {
            return Success(_repo.GetPatientOrderDetails(patientid));
        }
    }
}