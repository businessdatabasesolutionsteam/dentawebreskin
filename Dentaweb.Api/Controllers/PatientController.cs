﻿using BaseLibrary;
using Dentaweb.Api.Contracts;
using Dentaweb.Data.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dentaweb.Api.Controllers
{
    [Route("api/patient")]
    [ApiController]
    public class PatientController : BaseAPIController
    {
        private readonly IPatientRepository _repo;
        private readonly IInsuranceRepository _insurancerepo;
        private readonly ILogger<PatientController> _log;

        public PatientController(IPatientRepository repo, IInsuranceRepository insurancerepo, ILogger<PatientController> log)
        {
            _repo = repo;
            _insurancerepo = insurancerepo;
            _log = log;            
        }

        /// <summary>
        ///     Get patients
        /// </summary>
        /// <param name="search">search string</param>
        /// <returns>List of patients matching search string</returns>
        /// <response code="200">Success</response>
        /// <response code="404">No office exists with the id</response>
        [HttpGet("list/{search}")]
        public IActionResult Search(string search) //<IEnumerable<PatientDto>>
        {
            return Success(_repo.GetPatients(search));
        }

        /// <summary>
        ///     Get information about a single patient
        /// </summary>
        /// <param name="id">PatientId</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{id}", Name = "GetPatient")]
        public IActionResult Get(int id)
        {
            return Success(_repo.GetPatient(id));
        }

        /// <summary>
        ///     Get all notes for a single patient
        /// </summary>
        /// <param name="patientid">PatientId</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{patientid}/notes")]
        public IActionResult GetNotes(int patientid)
        {
            return Success(_repo.GetNotes(patientid));
        }

        /// <summary>
        ///     Get all appointments for a single patient
        /// </summary>
        /// <param name="patientid">PatientId</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{patientid}/apointments")]
        public IActionResult GetAppointments(int patientid)
        {
            return Success(_repo.GetAppointments(patientid));
        }

        /// <summary>
        ///     Get all insurance policies for a single patient
        /// </summary>
        /// <param name="patientid">PatientId</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{patientid}/insurancepolicies")]
        public IActionResult GetInsurancePolicies(int patientid)
        {
            return Success(_insurancerepo.GetInsurancePolicies(patientid));
        }

        /// <summary>
        ///     Get all insurance preauthorizations for a single patient
        /// </summary>
        /// <param name="patientid">PatientId</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{patientid}/insurancepreauthorizations")]
        public IActionResult GetInsurancePreAuthorizations(int patientid)
        {
            return Success(_insurancerepo.GetInsurancePreAuthorizations(patientid));
        }

        /// <summary>
        ///     Get all insurance benefits for a single patient
        /// </summary>
        /// <param name="patientid">PatientId</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{patientid}/insurancebenefits")]
        public IActionResult GetInsuranceBenefits(int patientid)
        {
            return Success(_insurancerepo.GetBenefitsRundowns(patientid));
        }

        /// <summary>
        ///     Create a new patient
        /// </summary>
        /// <param name="value"></param>
        /// <response code="201">Patient created</response>
        [HttpPost]
        public IActionResult PostPatient(PatientDto value)
        {
            var id = _repo.Create(value);

            return CreatedAtRoute("GetPatient", new { id }, id); // todo, new { CreatedBy = "testuser@test.com", CreatedOn = DateTime.UtcNow }
                                                                    // or return created Patient object
        }

        /// <summary>
        ///     Update a patient
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <response code="200">Patient updated</response>
        /// <response code="404">No patient exists with the id</response>
        [HttpPut("{id}")]
        public IActionResult PutPatient(int id, PatientDto value)
        {
            if (_repo.Update(id, value))
            {
                return Success();
            }
            else
            {
                return NotFound($"No patient exists with the id '{id}'.");
            }            
        }

        /// <summary>
        ///     Delete the patient with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">Patient deleted</response>
        /// <response code="404">No patient exists with the id</response>
        [HttpDelete("{id}")]
        public IActionResult DeletePatient(int id)
        {
            if (_repo.Delete(id))
            {
                return Success();
            }
            else
            {
                return NotFound($"No patient exists with the id '{id}'.");
            }
        }
    }
}
