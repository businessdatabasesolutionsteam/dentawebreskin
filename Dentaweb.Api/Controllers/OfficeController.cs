﻿using BaseLibrary;
using BaseLibrary.Exceptions;
using Dentaweb.Data.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace Dentaweb.Api.Controllers
{
    [Route("api/office")]
    [ApiController]
    public class OfficeController : BaseAPIController
    {
        private readonly IOfficeRepository _repo;
        private readonly ILogger<OfficeController> _log;

        public OfficeController(IOfficeRepository repo, ILogger<OfficeController> log)
        {
            _repo = repo;
            _log = log;
        }

        /// <summary>
        ///     Get office information
        /// </summary>
        /// <param name="officeid">OfficeId</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="404">No office exists with the id</response>
        [HttpGet("{officeid}")]
        public IActionResult GetOffice(int officeid) // todo if users are tied to an office, we could get office id from session
        {
            try
            {
                var office = _repo.GetOffice(officeid);
                return Success(office);
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        ///     Get a list of doctors for the office
        /// </summary>
        /// <param name="officeid">OfficeId</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="404">No office exists with the id</response>
        [HttpGet("{officeid}/doctors")]
        public IActionResult GetDoctors(int officeid)
        {
            try
            {
                var drs = _repo.GetDoctors(officeid).ToList(); //deferred execution would prevent catching error
                return Success(drs);
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}