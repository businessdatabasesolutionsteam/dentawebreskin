﻿using BaseLibrary;
using BaseLibrary.Enums;
using BaseLibrary.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dentaweb.Api.Controllers
{
    [Route("api/general")]
    [ApiController]
    public class GeneralController : BaseAPIController
    {
        private readonly ILogger<GeneralController> _log;

        public GeneralController(ILogger<GeneralController> log)
        {
            _log = log;
        }

        #region Enums
        /// <summary>
        ///     List of Marital Statuses
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("maritalstatuses")]
        public IActionResult GetMaritalStatuses()
        {
            var statuses = EnumHelper.Enumerate<MaritalStatus>();
            return Success(statuses);
        }

        /// <summary>
        ///     List of Races
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("races")]
        public IActionResult GetRaces()
        {
            var races = EnumHelper.Enumerate<Ethnicity>();
            return Success(races);
        }

        /// <summary>
        ///     List of Ethnicities (todo same as Race)
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("ethnicities")]
        public IActionResult GetEthnicities()
        {
            var races = EnumHelper.Enumerate<Ethnicity>();
            return Success(races);
        }

        /// <summary>
        ///     List of Referral Sources
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("referralsources")]
        public IActionResult GetReferralSources()
        {
            var sources = EnumHelper.Enumerate<ReferralSource>();
            return Success(sources);
        }

        /// <summary>
        ///     List of Student Statuses
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("studentstatuses")]
        public IActionResult GetStudentStatuses()
        {
            var statuses = EnumHelper.Enumerate<StudentStatus>();
            return Success(statuses);
        }

        /// <summary>
        ///     List of Personal Titles
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("titles")]
        public IActionResult GetTitles()
        {
            var titles = EnumHelper.Enumerate<PersonalTitle>();
            return Success(titles);
        }
        #endregion
    }
}