﻿using BaseLibrary;
using BaseLibrary.Enums;
using BaseLibrary.Exceptions;
using BaseLibrary.Helpers;
using Dentaweb.Api.Contracts;
using Dentaweb.Data.Enums;
using Dentaweb.Data.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dentaweb.Api.Controllers
{
    [Route("api/insurance")]
    [ApiController]
    public class InsuranceController : BaseAPIController
    {
        private readonly IInsuranceRepository _repo;
        private readonly ILogger<InsuranceController> _log;

        public InsuranceController(IInsuranceRepository repo, ILogger<InsuranceController> log)
        {
            _repo = repo;
            _log = log;
        }

        /// <summary>
        ///     Get a single insurance policy by id
        /// </summary>
        /// <param name="id">policy Id</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{id}", Name = "GetInsurancePolicy")]
        public IActionResult GetInsurancePolicy(int id)
        {
            return Success(_repo.GetInsurancePolicy(id));
        }

        /// <summary>
        ///     Create a new insurance policy
        /// </summary>
        /// <param name="value"></param>
        /// <response code="201">Insurance created</response>
        [HttpPost] // todo consider ("patient/{patientid}/insurancepolicy")]
        public IActionResult PostInsurancePolicy(InsurancePolicyDto value)
        {
            try
            {
                var id = _repo.CreateInsurancePolicy(value);
                return CreatedAtRoute("GetInsurancePolicy", new { id }, id);
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        ///     Update an Insurance
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <response code="200">Insurance updated</response>
        /// <response code="404">No Insurance exists with the id</response>
        [HttpPut("{id}")]
        public IActionResult PutInsurancePolicy(int id, InsurancePolicyDto value)
        {
            if (_repo.UpdateInsurancePolicy(id, value)) // todo EntityNotFoundException
            {
                return Success();
            }
            else
            {
                return NotFound($"No Insurance exists with the id '{id}'.");
            }
        }

        /// <summary>
        ///     Delete the insurance policy with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">Insurance policydeleted</response>
        /// <response code="404">No insurance policy exists with the id</response>
        [HttpDelete("{id}")]
        public IActionResult DeleteInsurancePolicy(int id)
        {
            if (_repo.DeleteInsurancePolicy(id)) // todo EntityNotFoundException
            {
                return Success();
            }
            else
            {
                return NotFound($"No Insurance policy exists with the id '{id}'.");
            }
        }

        /// <summary>
        ///     List of insurance companies
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("insurancecompanies")]
        public IActionResult GetInsuranceCompanyList()
        {
            return Success(_repo.GetInsuranceCompanies());
        }

        /// <summary>
        ///     List of insurance plans
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("insuranceplans")]
        public IActionResult GetInsurancePlanList()
        {
            return Success(_repo.GetInsurancePlans());
        }

        /// <summary>
        ///     List of insurance policy types
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("insurancepolicytypes")]
        public IActionResult GetInsurancePolicyTypes()
        {
            var types = EnumHelper.Enumerate<InsurancePolicyType>();
            return Success(types);
        }

        /// <summary>
        ///     List of insurance relationship types
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("insurancerelationshiptypes")]
        public IActionResult GetInsuranceRelationships()
        {
            var types = EnumHelper.Enumerate<InsuranceRelationship>();
            return Success(types);
        }
    }
}