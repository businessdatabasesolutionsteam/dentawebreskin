﻿using BaseLibrary;
using BaseLibrary.Enums;
using BaseLibrary.Exceptions;
using BaseLibrary.Helpers;
using Dentaweb.Api.Contracts;
using Dentaweb.Data.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dentaweb.Api.Controllers
{
    [Route("api/insurancebenefits")]
    [ApiController]
    public class InsuranceBenefitsController : BaseAPIController
    {
        private readonly IInsuranceRepository _repo;
        private readonly ILogger<InsuranceBenefitsController> _log;

        public InsuranceBenefitsController(IInsuranceRepository repo, ILogger<InsuranceBenefitsController> log)
        {
            _repo = repo;
            _log = log;
        }

        /// <summary>
        ///     Get a single benefits rundown by id
        /// </summary>
        /// <param name="id">benefits Id</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{id}", Name = "GetBenefitsRundown")]
        public IActionResult GetBenefitsRundown(int id)
        {
            return Success(_repo.GetBenefitsRundown(id));
        }

        /// <summary>
        ///     Create a new benefits rundown
        /// </summary>
        /// <param name="value"></param>
        /// <response code="201">Benefits Rundown created</response>
        [HttpPost]
        public IActionResult PostBenefitsRundown(BenefitsDto value)
        {
            try
            {
                var id = _repo.CreateBenefits(value);
                return CreatedAtRoute("GetBenefitsRundown", new { id }, id);
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        ///     Update a Benefits Rundown
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <response code="200">Benefits Rundown updated</response>
        /// <response code="404">No BenefitsRundown exists with the id</response>
        [HttpPut("{id}")]
        public IActionResult PutBenefitsRundown(int id, BenefitsDto value)
        {
            if (_repo.UpdateBenefits(id, value)) // todo EntityNotFoundException
            {
                return Success();
            }
            else
            {
                return NotFound($"No benefits rundown exists with the id '{id}'.");
            }
        }

        /// <summary>
        ///     Delete the BenefitsRundown with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">BenefitsRundown deleted</response>
        /// <response code="404">No BenefitsRundown exists with the id</response>
        [HttpDelete("{id}")]
        public IActionResult DeleteBenefitsRundown(int id)
        {
            if (_repo.DeleteBenefits(id)) // todo EntityNotFoundException
            {
                return Success();
            }
            else
            {
                return NotFound($"No benefits rundown exists with the id '{id}'.");
            }
        }
    }
}