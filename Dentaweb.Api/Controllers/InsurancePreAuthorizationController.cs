﻿using BaseLibrary;
using BaseLibrary.Enums;
using BaseLibrary.Exceptions;
using BaseLibrary.Helpers;
using Dentaweb.Api.Contracts;
using Dentaweb.Data.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dentaweb.Api.Controllers
{
    [Route("api/insurancepreauth")]
    [ApiController]
    public class InsurancePreAuthorizationController : BaseAPIController
    {
        private readonly IInsuranceRepository _repo;
        private readonly ILogger<InsurancePreAuthorizationController> _log;

        public InsurancePreAuthorizationController(IInsuranceRepository repo, ILogger<InsurancePreAuthorizationController> log)
        {
            _repo = repo;
            _log = log;
        }

        /// <summary>
        ///     Get a single InsurancePreAuthorization by id
        /// </summary>
        /// <param name="id">PreAuth Id</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{id}", Name = "GetInsurancePreAuthorization")]
        public IActionResult GetInsurancePreAuthorization(int id)
        {
            return Success(_repo.GetInsurancePreAuthorization(id));
        }

        /// <summary>
        ///     Create a new InsurancePreAuthorization
        /// </summary>
        /// <param name="value"></param>
        /// <response code="201">InsurancePreAuthorization created</response>
        [HttpPost] // todo consider ("patient/{patientid}/InsurancePreAuthorization")]
        public IActionResult PostInsurancePreAuthorization(InsurancePreAuthorizationDto value)
        {
            try
            {
                var id = _repo.CreateInsurancePreAuthorization(value);
                return CreatedAtRoute("GetInsurancePreAuthorization", new { id }, id);
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        ///     Update an InsurancePreAuthorization
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <response code="200">InsurancePreAuthorization updated</response>
        /// <response code="404">No InsurancePreAuthorization exists with the id</response>
        [HttpPut("{id}")]
        public IActionResult PutInsurancePreAuthorization(int id, InsurancePreAuthorizationDto value)
        {
            if (_repo.UpdateInsurancePreAuthorization(id, value)) // todo EntityNotFoundException
            {
                return Success();
            }
            else
            {
                return NotFound($"No InsurancePreAuthorization exists with the id '{id}'.");
            }
        }

        /// <summary>
        ///     Delete the InsurancePreAuthorization with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">InsurancePreAuthorization deleted</response>
        /// <response code="404">No InsurancePreAuthorization exists with the id</response>
        [HttpDelete("{id}")]
        public IActionResult DeleteInsurancePreAuthorization(int id)
        {
            if (_repo.DeleteInsurancePreAuthorization(id)) // todo EntityNotFoundException
            {
                return Success();
            }
            else
            {
                return NotFound($"No InsurancePreAuthorization exists with the id '{id}'.");
            }
        }
    }
}