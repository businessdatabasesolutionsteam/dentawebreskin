﻿using BaseLibrary;
using BaseLibrary.Exceptions;
using Dentaweb.Api.Contracts;
using Dentaweb.Data.Enums;
using Dentaweb.Data.Patients;
using Dentaweb.Data.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Net.Http;

namespace Dentaweb.Api.Controllers
{
    [Route("api/image")]
    [ApiController]
    public class ImageController : BaseAPIController
    {
        private readonly ILogger<ImageController> _log;

        public ImageController(ILogger<ImageController> log)
        {
            _log = log;
        }

        /// <summary>
        ///     Get a set of images for a patient
        /// </summary>
        /// <param name="patientid">patient id</param>
        /// <param name="imagesetid">Id for a set of images</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{patientid}/{imagesetid}", Name = "GetImageSet")]
        public IActionResult GetImageSet(int patientid, int imagesetid)
        {
            // todo set up repo
            var imageset = new PatientImageSet()
            {
                CaseId = "abc123",
                PatiendId = patientid,
                PatientImageSetId = imagesetid
            };
            return Success(imageset);
        }

        /// <summary>
        ///     Create a new set of images of a patient
        /// </summary>
        /// <param name="patientid">patient id</param>
        /// <param name="frontrelaxed">image</param>
        /// <param name="profile">image</param>
        /// <param name="submentovertex">image</param>
        /// <param name="frontfullbodyposture">image</param>
        /// <param name="posteriorfullbodyposture">image</param>
        /// <response code="201">Image set created</response>
        [HttpPost("{patientid}")]
        public IActionResult PostImageSet(
            int patientid,
            IFormFile frontrelaxed, 
            IFormFile profile, 
            IFormFile submentovertex, 
            IFormFile frontfullbodyposture, 
            IFormFile posteriorfullbodyposture)
        {
            // todo need to set up repo depending where/how images are saved
            return CreatedAtRoute("GetImageSet", new { patientid, imagesetid = 6548 }, 6548);
        }

        /// <summary>
        ///     Get a set of intra oral images for a patient
        /// </summary>
        /// <param name="patientid">patient id</param>
        /// <param name="imagesetid">Id for a set of images</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{patientid}/intraoral/{imagesetid}", Name = "GetIntraOralImageSet")]
        public IActionResult GetIntraOralImageSet(int patientid, int imagesetid)
        {
            // todo set up repo
            var imageset = new IntraOralImageSet()
            {
                CaseId = "abc123",
                PatiendId = patientid,
                PatientImageSetId = imagesetid
            };
            return Success(imageset);
        }

        /// <summary>
        ///     Create a new set of intra oral images of a patient
        /// </summary>
        /// <param name="patientid">patient id</param>
        /// <param name="anteriorimage">image</param>
        /// <param name="leftbuccal">image</param>
        /// <param name="rightbuccal">image</param>
        /// <param name="upperarch">image</param>
        /// <param name="lowerarch">image</param>
        /// <param name="lingualfrenum">image</param>
        /// <response code="201">Image set created</response>
        [HttpPost("{patientid}/intraoral")]
        public IActionResult PostIntraOralImageSet(
            int patientid,
            IFormFile anteriorimage,
            IFormFile leftbuccal,
            IFormFile rightbuccal,
            IFormFile upperarch,
            IFormFile lowerarch,
            IFormFile lingualfrenum)
        {
            // todo need to set up repo depending where/how images are saved
            return CreatedAtRoute("GetIntraOralImageSet", new { patientid, imagesetid = 31248 }, 31248);
        }

        /// <summary>
        ///     Get a set of CBCT images for a patient
        /// </summary>
        /// <param name="patientid">patient id</param>
        /// <param name="imagesetid">Id for a set of images</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{patientid}/cbct/{imagesetid}", Name = "GetCbctImageSet")]
        public IActionResult GetCbctImageSet(int patientid, int imagesetid)
        {
            // todo set up repo
            var imageset = new CbctImageSet()
            {
                CaseId = "abc123",
                PatiendId = patientid,
                PatientImageSetId = imagesetid
            };
            return Success(imageset);
        }

        /// <summary>
        ///     Create a new set of CBCT images of a patient
        /// </summary>
        /// <param name="patientid">patient id</param>
        /// <param name="cbctscan">image</param>
        /// <param name="lateralceph">image</param>
        /// <param name="pano">image</param>
        /// <param name="ceph">image</param>
        /// <param name="tomographleft">image</param>
        /// <param name="tomographright">image</param>
        /// <response code="201">Image set created</response>
        [HttpPost("{patientid}/cbct")]
        public IActionResult PostCbctImageSet(
            int patientid,
            IFormFile cbctscan,
            IFormFile lateralceph,
            IFormFile pano,
            IFormFile ceph,
            IFormFile tomographleft,
            IFormFile tomographright)
        {
            // todo need to set up repo depending where/how images are saved
            return CreatedAtRoute("GetCbctImageSet", new { patientid, imagesetid = 4118 }, 4118);
        }

        /// <summary>
        ///     Get a set of 3d occlusion images for a patient
        /// </summary>
        /// <param name="patientid">patient id</param>
        /// <param name="imagesetid">Id for a set of images</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{patientid}/occl3d/{imagesetid}", Name = "GetOcclusion3dImageSet")]
        public IActionResult GetOcclusion3dImageSet(int patientid, int imagesetid)
        {
            // todo set up repo
            var imageset = new Occlusion3dImageSet()
            {
                CaseId = "abc123",
                PatiendId = patientid,
                PatientImageSetId = imagesetid
            };
            return Success(imageset);
        }

        /// <summary>
        ///     Create a new set of 3d occlusion images of a patient
        /// </summary>
        /// <param name="patientid">patient id</param>
        /// <param name="upperarch">image</param>
        /// <param name="lowerarch">image</param>
        /// <param name="naturalbite">image</param>
        /// <param name="constructedbite">image</param>
        /// <response code="201">Image set created</response>
        [HttpPost("{patientid}/occl3d")]
        public IActionResult PostOcclusion3dImageSet(
            int patientid,
            IFormFile upperarch,
            IFormFile lowerarch,
            IFormFile naturalbite,
            IFormFile constructedbite)
        {
            // todo need to set up repo depending where/how images are saved
            return CreatedAtRoute("GetOcclusion3dImageSet", new { patientid, imagesetid = 7835 }, 7835);
        }

        /// <summary>
        ///     Get a set of 2d occlusion images for a patient
        /// </summary>
        /// <param name="patientid">patient id</param>
        /// <param name="imagesetid">Id for a set of images</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{patientid}/occl2d/{imagesetid}", Name = "GetOcclusion2dImageSet")]
        public IActionResult GetOcclusion2dImageSet(int patientid, int imagesetid)
        {
            // todo set up repo
            var imageset = new Occlusion2dImageSet()
            {
                CaseId = "abc123",
                PatiendId = patientid,
                PatientImageSetId = imagesetid,
                LeftMolarRelationship = OcclusionRelationship.ClassI,
                RightMolarRelationship = OcclusionRelationship.ClassIII,
                ImpressionMadeBy = "Dan Smith",
            };
            return Success(imageset);
        }

        /// <summary>
        ///     Create a new set of 2d occlusion images of a patient
        /// </summary>
        /// <param name="patientid">patient id</param>
        /// <param name="upperarch">image</param>
        /// <param name="lowerarch">image</param>
        /// <param name="frontocclusion">image</param>
        /// <param name="leftocclusion">image</param>
        /// <param name="rightocclusion">image</param>
        /// <param name="rightmolarrelationship">Class I, Class II, Class III</param>
        /// <param name="leftmolarrelationship">Class I, Class II, Class III</param>
        /// <param name="impressionmadeby">Staff Member who took the impression</param>
        /// <response code="201">Image set created</response>
        [HttpPost("{patientid}/occl2d")]
        public IActionResult PostOcclusion2dImageSet(
            int patientid,
            IFormFile upperarch,
            IFormFile lowerarch,
            IFormFile frontocclusion,
            IFormFile leftocclusion,
            IFormFile rightocclusion,
            string rightmolarrelationship,
            string leftmolarrelationship,
            string impressionmadeby)
        {
            // todo need to set up repo depending where/how images are saved
            return CreatedAtRoute("GetOcclusion2dImageSet", new { patientid, imagesetid = 1279 }, 1279);
        }
    }
}