﻿using BaseLibrary;
using BaseLibrary.Exceptions;
using Dentaweb.Api.Contracts;
using Dentaweb.Data.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dentaweb.Api.Controllers
{
    [Route("api/appointment")]
    [ApiController]
    public class AppointmentController : BaseAPIController
    {
        private readonly IPatientRepository _repo;
        private readonly ILogger<AppointmentController> _log;

        public AppointmentController(IPatientRepository repo, ILogger<AppointmentController> log)
        {
            _repo = repo;
            _log = log;
        }

        /// <summary>
        ///     Get a single appointment by id
        /// </summary>
        /// <param name="id">Appointment Id</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{id}", Name = "GetAppointment")]
        public IActionResult GetAppointment(int id)
        {
            return Success(_repo.GetAppointment(id));
        }

        /// <summary>
        ///     Create a new appointment
        /// </summary>
        /// <param name="value"></param>
        /// <response code="201">Appointment created</response>
        [HttpPost] // todo consider ("patient/{patientid}/appointment")]
        public IActionResult PostAppointment(AppointmentDto value)
        {
            try
            {
                var id = _repo.CreateAppointment(value);
                return CreatedAtRoute("GetAppointment", new { id }, id);
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        ///     Update an Appointment
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <response code="200">Appointment updated</response>
        /// <response code="404">No Appointment exists with the id</response>
        [HttpPut("{id}")]
        public IActionResult PutAppointment(int id, AppointmentDto value)
        {
            if (_repo.UpdateAppointment(id, value)) // todo EntityNotFoundException
            {
                return Success();
            }
            else
            {
                return NotFound($"No Appointment exists with the id '{id}'.");
            }
        }

        /// <summary>
        ///     Delete the Appointment with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">Appointment deleted</response>
        /// <response code="404">No Appointment exists with the id</response>
        [HttpDelete("{id}")]
        public IActionResult DeleteAppointment(int id)
        {
            if (_repo.DeleteAppointment(id)) // todo EntityNotFoundException
            {
                return Success();
            }
            else
            {
                return NotFound($"No appointment exists with the id '{id}'.");
            }
        }
    }
}