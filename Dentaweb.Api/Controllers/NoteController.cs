﻿using BaseLibrary;
using BaseLibrary.Exceptions;
using Dentaweb.Api.Contracts;
using Dentaweb.Data.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dentaweb.Api.Controllers
{
    [Route("api/note")]
    [ApiController]
    public class NoteController : BaseAPIController
    {
        private readonly IPatientRepository _repo;
        private readonly ILogger<NoteController> _log;

        public NoteController(IPatientRepository repo, ILogger<NoteController> log)
        {
            _repo = repo;
            _log = log;
        }

        /// <summary>
        ///     Get a single note by id
        /// </summary>
        /// <param name="id">Note Id</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet("{id}", Name ="GetNote")]
        public IActionResult GetNote(int id)
        {
            return Success(_repo.GetNote(id));
        }

        /// <summary>
        ///     Create a new note
        /// </summary>
        /// <param name="value"></param>
        /// <response code="201">Note created</response>
        [HttpPost] // todo consider ("patient/{patientid}/note")]
        public IActionResult PostNote(PatientNoteDto value)
        {
            try
            {
                var id = _repo.CreateNote(value);
                return CreatedAtRoute("GetNote", new { id }, id);
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        ///     Update a note
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <response code="200">Note updated</response>
        /// <response code="404">No note exists with the id</response>
        [HttpPut("{id}")]
        public IActionResult PutNote(int id, PatientNoteDto value)
        {
            if (_repo.UpdateNote(id, value)) // todo EntityNotFoundException
            {
                return Success();
            }
            else
            {
                return NotFound($"No note exists with the id '{id}'.");
            }
        }

        /// <summary>
        ///     Delete the note with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">Note deleted</response>
        /// <response code="404">No note exists with the id</response>
        [HttpDelete("{id}")]
        public IActionResult DeleteNote(int id)
        {
            if (_repo.DeleteNote(id)) // todo EntityNotFoundException
            {
                return Success();
            }
            else
            {
                return NotFound($"No note exists with the id '{id}'.");
            }
        }
    }
}