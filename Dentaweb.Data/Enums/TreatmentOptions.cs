﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Enums
{
    public enum TreatmentOptions
    {
        VivosGuides = 1,
        FixedApplianceTherapy = 2,
        ClearAlligners = 3,
        RestorativeReconstruction = 4,
        Other = 5
    }
}
