﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Enums
{
    public enum OcclusionRelationship
    {
        ClassI = 1,
        ClassII = 2,
        ClassIII = 3,
    }
}
