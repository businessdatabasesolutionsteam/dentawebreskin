﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Enums
{
    public enum InsurancePolicyType
    {
        Primary = 1,
        Secondary = 2
    }
}
