﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseLibrary.Enums
{
    public enum ReferralSource
    {
        WalkIn = 1,
        Web = 2,
        Other = 3,
    }
}
