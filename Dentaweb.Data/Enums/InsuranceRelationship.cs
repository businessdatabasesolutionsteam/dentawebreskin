﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Enums
{
    public enum InsuranceRelationship
    {
        Primary = 1,
        Spouse = 2,
        Child = 3,
        Other = 4
    }
}
