﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Enums
{
    public enum InsuranceType
    {
        PPO = 1,
        HMO = 2
    }
}
