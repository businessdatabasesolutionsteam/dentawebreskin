﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseLibrary.Enums
{
    public enum StudentStatus
    {
        HighSchool = 1,
        SomeCollege = 2,
        CollegeGraduate = 3,
    }
}
