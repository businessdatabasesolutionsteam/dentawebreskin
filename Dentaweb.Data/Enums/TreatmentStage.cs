﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Enums
{
    public enum TreatmentStage
    {
        PreTreatment = 1,
        MidTreatment = 2,
        PostTreatment = 3,
    }
}
