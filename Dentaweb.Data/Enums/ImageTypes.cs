﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Enums
{
    public enum PatientImageType
    {
        // Patient
        FrontRelaxed = 1,
        Profile = 2,
        SubmentoVertex = 3,
        FrontFullBodyPosture = 4,
        PosteriorFullBodyPosture = 5,

        // Patient Intra Oral
        Anterior = 6,
        LeftBuccal = 7,
        RightBuccal = 8,
        UpperArch = 9,
        LowerArch = 10,
        LingualFrenum = 11,

        // CBCT
        CBCTScan = 12,
        LateralCeph = 13,
        Pano = 14,
        Ceph = 15,
        TomographLeft = 16,
        TomographRight = 17,

        // Occlusion 3D/2D
        OcclusionUpperArch = 18,
        OcclusionLowerArch = 19,
        OcclusionNaturalBite = 20,
        OcclusionConstructedBite = 21,
        OcclusionFront = 22,
        OcclusionLeft = 23,
        OcclusionRight = 24,
    }
}
