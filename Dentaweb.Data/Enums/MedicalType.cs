﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Enums
{
    public enum MedicalType
    {
        Medical = 1,
        Dental = 2,
        Ortho = 3
    }
}
