﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Enums
{
    public enum TreatmentSystem
    {
        VivosSingleArchDnaSystem = 1,
        VivosUlArchDnaSystem = 2,
        VivosMrnaSystem = 3,
        VivosGuideSystem = 4,
        Other = 5
    }
}
