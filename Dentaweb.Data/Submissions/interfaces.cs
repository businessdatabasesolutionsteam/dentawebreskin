﻿using Dentaweb.Data.Enums;
using System;
using System.Collections.Generic;
using System.Text;

// todo need to find out the data type for most of this
namespace Dentaweb.Data.Submissions
{
    public interface ISubmissionHealthHistory
    {
        //Patient Health History ID { get; set; }
        int PatientId { get; set; }
        int CaseId { get; set; }
        decimal BMI { get; set; }
        string Epworth { get; set; }
        string BPM { get; set; }
        DateTime SleepTestDate { get; set; }
        string PahiScore { get; set; }
        TreatmentStage TreatmentStage { get; set; }
        string HomeSleepTest { get; set; }
        bool PatientHasTMD { get; set; }
        string TMDText { get; set; }
        MallampatiScore MallampatiScore { get; set; }
        bool HasHistoryOfOrthodontics { get; set; }
        bool HasHistoryOfMedicalDentalSurgeries { get; set; }
        string HasHistoryOfMedicalDentalSurgeriesText { get; set; }
        bool HasHistoryOfHypothyroidism { get; set; }
        bool CurrentlyOnThyroidMed { get; set; }
    }

    //public interface ISubmissionImageSet { } todo these seem to be the same as the PatientImageSets
    //public interface ISubmissionIntraOralImagesSet { }
    //public interface ISubmissionCBCTImageSet { }
    //public interface ISubmissionOccl3dImageSet { }
    //public interface ISubmissionOccl2dImageSet { }

    public interface ISubmissionToothCharting
    {
        // int Teeth Charting ID { get; set; }
        int PatientId { get; set; }
        string CaseId { get; set; }
        int ToothNumber { get; set; }
        string MissingOrExtracted { get; set; }
        string Bridge { get; set; }
        string Crown { get; set; }
        string Implants { get; set; }
        string Impacted { get; set; }
        string Deciduous { get; set; }
    }

    public interface ISubmissionArchAnalysis
    {
        //ArchAnalysisId { get; set; }
        int PatientId { get; set; }
        string CaseId { get; set; }
        string AMeasurmentInput { get; set; }
        string BMeasurementInput { get; set; }
        string BDiscrepancyCalculation { get; set; }
        string CMeasurmentInput { get; set; }
        string CDiscrepancyCalculation { get; set; }
        string B1MeasurmentInput { get; set; }
        string B1DiscrepancyCalculation { get; set; }
        string C1MeasurmentInput { get; set; }
        string C1DiscrepancyCalculation { get; set; }
    }

    public interface ISubmissionPatientObjectives
    {
        //Patient Objectives ID { get; set; }
        int PatientId { get; set; }
        string CaseId { get; set; }
        string ComplaintSleep { get; set; }
        string ComplaintFunction { get; set; }
        string ComplaintPain { get; set; }
        string ComplaintLooks { get; set; }
        string ComplaintsDetails { get; set; }
        string InitDiagRetrusiveSleepBruxism { get; set; }
        string InitDiagPreviouslyDiagnosedDSA { get; set; }
        string InitDiagPreviouslyDiagnosedTMD { get; set; }
        string InitDiagSuspectedUnddiagnosedTmdBsb { get; set; }
        string InitDiagCranofacialHipoplasia { get; set; }
        string InitDiagOther { get; set; }
        string InitDiagDetails { get; set; }
        string InitTreatObjImproveOverbiteOverjeTtoWnl { get; set; }
        string InitTreatObjReduceGummySmileTendency { get; set; }
        string InitTreatObjRecaptureUpperPremolarSpaces { get; set; }
        string InitTreatObjCorrectMidlineDiscrepancy { get; set; }
        string InitTreatObjIncreaseTranspatialBoneWidth { get; set; }
        string InitTreatObjResolveUpperAnteriorCrowding { get; set; }
        string InitTreatObjResolveUpperAnteriorSpacing { get; set; }
        string InitTreatObjresolveLowerAnteriorCrowding { get; set; }
        string InitTreatObjResolveLowerAnteriorSpacing { get; set; }
        string InitTreatObjResolveLowerPosteriorSpacing { get; set; }
        string InitTreatObjRepositionMandibleAnteriorInferiorityInMidline { get; set; }
        string InitTreatObjrepositionMandibleAnteroSsuperiorlyInMidline { get; set; }
        string InitTreatObjRepositionMandibleInMidline { get; set; }
        string InitTreatObjRepositionMandibleSuperiorlynMidline { get; set; }
        string InitTreatObjRepositionMandibleAnteriorlyInMidline { get; set; }
        string InitTreatObjimproveUpperAirwayParametersNasal { get; set; }
        string InitTreatObjimproveUpperAirwayParametersRetroplatal { get; set; }
        string InitTreatObjimproveUpperAirwaPparametersRetroglossal { get; set; }
        string InitTreatObjCorrectCrossbiteAnteriorTendency { get; set; }
        string InitTreatObjCorrectCrossbitePosteriorTendency { get; set; }
        string InitTreatObjOther { get; set; }
        string InitTreatObjOtherDetails { get; set; }
        string InitTreatPlanReviewTmjStatusRadiologistOpinion { get; set; }
        string InitTreatPlanSuggestOvernightSleepStudyHstSleepMdOpinion { get; set; }
        string InitTreatPlanSuggestAtlasOrthogonistNUCCAChiropracticOpinion { get; set; }
        string InitTreatPlanSuggestHhealtAndNutritionalCounselingForBmiManagement { get; set; }
        string InitTreatPlanConsidsPriorToTreatTmjStabilization { get; set; }
        string InitTreatPlanConsidsPriorToTreatAddressPeriodontalDesease { get; set; }
        string InitTreatPlanConsidsPriorToTreatRecommendLingualFernectomy { get; set; }
        string InitTreatPlanConsidsPriorToTreatSurgicalRemovalOfMandibularTori { get; set; }
        TreatmentSystem InitTreatPlanSuggestedApplianceTreatmentSystem { get; set; }
        string EstTreatTimeEstimatedTreatementTimeInMonths { get; set; }
        TreatmentOptions EstTreatTimeInitialPhase2TreatmentOptions { get; set; }
        string EstTreatTimeOtherDetails { get; set; }
    }

    public interface ISubmissionMiscDocs
    {
        //Misc Doc ID { get; set; }
        int PatientId { get; set; }
        string CaseId { get; set; }
        string Document { get; set; }
    }

    public interface ISubmissionOrderDetails
    {
        //int OrderDetailsId { get; set; }
        int PatientId { get; set; }
        string CaseId { get; set; }
        string StandardDiagReview { get; set; }
        string RushCharges { get; set; }
        string SassouniPlus { get; set; }
        string RadiologyReport { get; set; }
        string Cardtype { get; set; }
        string CardNumber { get; set; }
        string CVV { get; set; } // todo can/should we persist this?
        string ExpirationDate { get; set; }
        string CardholderName { get; set; }
        bool SaveCardToAccount { get; set; }
    }
}
