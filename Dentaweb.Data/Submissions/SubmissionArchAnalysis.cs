﻿namespace Dentaweb.Data.Submissions
{
    public class SubmissionArchAnalysis : ISubmissionArchAnalysis
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public string CaseId { get; set; }
        public string AMeasurmentInput { get; set; }
        public string BMeasurementInput { get; set; }
        public string BDiscrepancyCalculation { get; set; }
        public string CMeasurmentInput { get; set; }
        public string CDiscrepancyCalculation { get; set; }
        public string B1MeasurmentInput { get; set; }
        public string B1DiscrepancyCalculation { get; set; }
        public string C1MeasurmentInput { get; set; }
        public string C1DiscrepancyCalculation { get; set; }
    }
}
