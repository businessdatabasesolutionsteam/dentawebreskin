﻿using Dentaweb.Data.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Submissions
{
    public class SubmissionPatientObjectives : ISubmissionPatientObjectives
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public string CaseId { get; set; }
        public string ComplaintSleep { get; set; }
        public string ComplaintFunction { get; set; }
        public string ComplaintPain { get; set; }
        public string ComplaintLooks { get; set; }
        public string ComplaintsDetails { get; set; }
        public string InitDiagRetrusiveSleepBruxism { get; set; }
        public string InitDiagPreviouslyDiagnosedDSA { get; set; }
        public string InitDiagPreviouslyDiagnosedTMD { get; set; }
        public string InitDiagSuspectedUnddiagnosedTmdBsb { get; set; }
        public string InitDiagCranofacialHipoplasia { get; set; }
        public string InitDiagOther { get; set; }
        public string InitDiagDetails { get; set; }
        public string InitTreatObjImproveOverbiteOverjeTtoWnl { get; set; }
        public string InitTreatObjReduceGummySmileTendency { get; set; }
        public string InitTreatObjRecaptureUpperPremolarSpaces { get; set; }
        public string InitTreatObjCorrectMidlineDiscrepancy { get; set; }
        public string InitTreatObjIncreaseTranspatialBoneWidth { get; set; }
        public string InitTreatObjResolveUpperAnteriorCrowding { get; set; }
        public string InitTreatObjResolveUpperAnteriorSpacing { get; set; }
        public string InitTreatObjresolveLowerAnteriorCrowding { get; set; }
        public string InitTreatObjResolveLowerAnteriorSpacing { get; set; }
        public string InitTreatObjResolveLowerPosteriorSpacing { get; set; }
        public string InitTreatObjRepositionMandibleAnteriorInferiorityInMidline { get; set; }
        public string InitTreatObjrepositionMandibleAnteroSsuperiorlyInMidline { get; set; }
        public string InitTreatObjRepositionMandibleInMidline { get; set; }
        public string InitTreatObjRepositionMandibleSuperiorlynMidline { get; set; }
        public string InitTreatObjRepositionMandibleAnteriorlyInMidline { get; set; }
        public string InitTreatObjimproveUpperAirwayParametersNasal { get; set; }
        public string InitTreatObjimproveUpperAirwayParametersRetroplatal { get; set; }
        public string InitTreatObjimproveUpperAirwaPparametersRetroglossal { get; set; }
        public string InitTreatObjCorrectCrossbiteAnteriorTendency { get; set; }
        public string InitTreatObjCorrectCrossbitePosteriorTendency { get; set; }
        public string InitTreatObjOther { get; set; }
        public string InitTreatObjOtherDetails { get; set; }
        public string InitTreatPlanReviewTmjStatusRadiologistOpinion { get; set; }
        public string InitTreatPlanSuggestOvernightSleepStudyHstSleepMdOpinion { get; set; }
        public string InitTreatPlanSuggestAtlasOrthogonistNUCCAChiropracticOpinion { get; set; }
        public string InitTreatPlanSuggestHhealtAndNutritionalCounselingForBmiManagement { get; set; }
        public string InitTreatPlanConsidsPriorToTreatTmjStabilization { get; set; }
        public string InitTreatPlanConsidsPriorToTreatAddressPeriodontalDesease { get; set; }
        public string InitTreatPlanConsidsPriorToTreatRecommendLingualFernectomy { get; set; }
        public string InitTreatPlanConsidsPriorToTreatSurgicalRemovalOfMandibularTori { get; set; }
        public TreatmentSystem InitTreatPlanSuggestedApplianceTreatmentSystem { get; set; }
        public string EstTreatTimeEstimatedTreatementTimeInMonths { get; set; }
        public TreatmentOptions EstTreatTimeInitialPhase2TreatmentOptions { get; set; }
        public string EstTreatTimeOtherDetails { get; set; }

        public IEnumerable<SubmissionPatientObjectivesAppliance> Appliances { get; set; }
    }

    public class SubmissionPatientObjectivesAppliance
    {
        public TreatmentSystem EstTreatTimeAppliance { get; set; }
        public int EstTreatTimeApproxNumMonthsInUse { get; set; }
        public string EstTreatTimeApproxPatientWearTimeHrsDay { get; set; }
        public int EstTreatTimeApproximateNumMonthsOfPassiveUse { get; set; }
    }
}
