﻿namespace Dentaweb.Data.Submissions
{
    public class SubmissionToothCharting : ISubmissionToothCharting
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public string CaseId { get; set; }
        public int ToothNumber { get; set; }
        public string MissingOrExtracted { get; set; }
        public string Bridge { get; set; }
        public string Crown { get; set; }
        public string Implants { get; set; }
        public string Impacted { get; set; }
        public string Deciduous { get; set; }
    }
}
