﻿namespace Dentaweb.Data.Submissions
{
    public class SubmissionOrderDetails : ISubmissionOrderDetails
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public string CaseId { get; set; }
        public string StandardDiagReview { get; set; }
        public string RushCharges { get; set; }
        public string SassouniPlus { get; set; }
        public string RadiologyReport { get; set; }
        public string Cardtype { get; set; }
        public string CardNumber { get; set; }
        public string CVV { get; set; }
        public string ExpirationDate { get; set; }
        public string CardholderName { get; set; }
        public bool SaveCardToAccount { get; set; }
    }
}
