﻿using Dentaweb.Data.Enums;
using System;

namespace Dentaweb.Data.Submissions
{
    public class SubmissionHealthHistory : ISubmissionHealthHistory
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public int CaseId { get; set; }
        public decimal BMI { get; set; }
        public string Epworth { get; set; }
        public string BPM { get; set; }
        public DateTime SleepTestDate { get; set; }
        public string PahiScore { get; set; }
        public TreatmentStage TreatmentStage { get; set; }
        public string HomeSleepTest { get; set; }
        public bool PatientHasTMD { get; set; }
        public string TMDText { get; set; }
        public MallampatiScore MallampatiScore { get; set; }
        public bool HasHistoryOfOrthodontics { get; set; }
        public bool HasHistoryOfMedicalDentalSurgeries { get; set; }
        public string HasHistoryOfMedicalDentalSurgeriesText { get; set; }
        public bool HasHistoryOfHypothyroidism { get; set; }
        public bool CurrentlyOnThyroidMed { get; set; }
    }
}
