﻿namespace Dentaweb.Data.Submissions
{
    public class SubmissionMiscDocs : ISubmissionMiscDocs
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public string CaseId { get; set; }
        public string Document { get; set; }
    }
}
