﻿using BaseLibrary.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Insurance
{
    public class Benefits : IBenefits, IPersistedEntity
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public string PatientInsuranceId { get; set; }
        public int InsuranceCompanyId { get; set; }
        public string RepresentativeName { get; set; }
        public string RepresentativeId { get; set; }
        public DateTime CoverageEffectiveDate { get; set; }
        public int BenefitYear { get; set; }
        public decimal DeductibleInNetworkIndividual { get; set; }
        public decimal DeductibleInNetworkFamily { get; set; }
        public bool InNetworkDeductibleMetIndividual { get; set; }
        public bool InNetworkDeductibleMetFamily { get; set; }
        public decimal InNetworkOutOfPocketMaxIndividual { get; set; }
        public decimal InNetworkOutOfPocketMaxFamily { get; set; }
        public bool InNetworkOutOfPocketMetIndividual { get; set; }
        public bool InNetworkOutOfPocketMetFamily { get; set; }
        public decimal OutOfNetworkDeductibleIndividual { get; set; }
        public decimal OutOfNetworkDeductibleFamily { get; set; }
        public bool OutOfNetworkDeductibleMetIndividual { get; set; }
        public bool OutOfNetworkDeductibleMetFamily { get; set; }
        public decimal OutOfNetworkOutOfPocketMaxIndividual { get; set; }
        public decimal OutOfNetworkOutOfPocketMaxFamily { get; set; }
        public bool OutOfNetworkOutOfPocketMetIndividual { get; set; }
        public bool OutOfNetworkOutOfPocketMetFamily { get; set; }
        public decimal CopayInNetworkSpecialistVisit { get; set; }
        public decimal CopayInNetworkPhysicalTherapyVisit { get; set; }
        public string PreAuthorizationDepartmentPhoneNumber { get; set; }
        public string PreAuthorizationDepartmentFaxNumber { get; set; }
        public bool PolicyhasBothInAndOutOfNetworkBenefits { get; set; }
        public bool DentalCoverage { get; set; }
        public bool OrthoCoverage { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
