﻿using BaseLibrary.Enums;
using Dentaweb.Data.Enums;
using System;

namespace Dentaweb.Data.Insurance
{
    public interface IInsurancePolicy
    {
        int PatientId { get; set; }
        int InsuranceCompanyId { get; set; }
        string InsuranceCompanyName { get; set; }
        InsuranceType InsuranceType { get; set; }
        InsurancePolicyType PolicyType { get; set; }

        decimal CoPayment { get; set; }
        DateTime CoverageEffectiveDate { get; set; }
        string EmergencyContactPersonName { get; set; }
        string EmergencyContactPhone { get; set; }
        string EmergencyContactRelation { get; set; }
        string EmployerAddress { get; set; }
        string EmployerGroupPolicyNumber { get; set; }
        string EmployerName { get; set; }
        string EmployerPhone { get; set; }
        string InsurancePlanId { get; set; }
        MedicalType MedicalType { get; set; }
        string PolicyHolderAddress1 { get; set; }
        string PolicyHolderAddress2 { get; set; }
        string PolicyHolderCellPhone { get; set; }
        string PolicyHolderCity { get; set; }
        string PolicyHolderCountry { get; set; }
        DateTime PolicyHolderDateofBirth { get; set; }
        string PolicyHolderEmail { get; set; }
        string PolicyHolderFirstName { get; set; }
        Gender PolicyHolderGender { get; set; }
        string PolicyHolderHomePhone { get; set; }
        string PolicyHolderLastName { get; set; }
        string PolicyHolderMiddleName { get; set; }
        string PolicyHolderState { get; set; }
        string PolicyHolderWorkPhone { get; set; }
        string PolicyHolderZipCode { get; set; }
        bool PolicyIsActive { get; set; }
        string PolicyNumberMemberID { get; set; }
        InsuranceRelationship RelationshipToPolicyHolder { get; set; }
        string ThirdPartyNumber { get; set; }
    }

    public interface IInsurancePreAuthorization
    {
        //int PreAuthId { get; set; }
        int PatientId { get; set; }
        int OfficeId { get; set; }
        string InsuranceId { get; set; }
        DateTime PreAuthDate { get; set; }
        int AppointmentId { get; set; }
        int ProviderId { get; set; }
        //decimal ApproxInsurancePay { get; set; }
        //decimal ApproxPatient Portion { get; set; }
        //decimal PatientActualCharge { get; set; }
        //string Remarks { get; set; }
        //DateTime ExpirationDate { get; set; }
        int ProductCode { get; set; } // todo this is a RundownCode
        PreAuthorizationType PreAuthorizationStatus { get; set; }
        string PreAuthConfirmationNumber { get; set; }
    }

    public interface IInsurancePlan
    {
        string PlanName { get; set; }
        int CompanyId { get; set; }
    }

    public interface IInsuranceCompany
    {
        InsuranceType InsuranceType { get; set; }
        int CompanyId { get; set; }
        string Name { get; set; }
        string Address { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Phone { get; set; }
        string ClaimPhone { get; set; }
        string PreAuthPhone { get; set; }
        string AppealPhone { get; set; }
    }

    public interface IBenefits
    {
        int PatientId { get; set; }
        string PatientInsuranceId { get; set; }
        int InsuranceCompanyId { get; set; }
        string RepresentativeName { get; set; }
        string RepresentativeId { get; set; }
        DateTime CoverageEffectiveDate { get; set; }
        int BenefitYear { get; set; }
        decimal DeductibleInNetworkIndividual { get; set; }
        decimal DeductibleInNetworkFamily { get; set; }
        bool InNetworkDeductibleMetIndividual { get; set; }
        bool InNetworkDeductibleMetFamily { get; set; }
        decimal InNetworkOutOfPocketMaxIndividual { get; set; }
        decimal InNetworkOutOfPocketMaxFamily { get; set; }
        bool InNetworkOutOfPocketMetIndividual { get; set; }
        bool InNetworkOutOfPocketMetFamily { get; set; }
        decimal OutOfNetworkDeductibleIndividual { get; set; }
        decimal OutOfNetworkDeductibleFamily { get; set; }
        bool OutOfNetworkDeductibleMetIndividual { get; set; }
        bool OutOfNetworkDeductibleMetFamily { get; set; }
        decimal OutOfNetworkOutOfPocketMaxIndividual { get; set; }
        decimal OutOfNetworkOutOfPocketMaxFamily { get; set; }
        bool OutOfNetworkOutOfPocketMetIndividual { get; set; }
        bool OutOfNetworkOutOfPocketMetFamily { get; set; }
        decimal CopayInNetworkSpecialistVisit { get; set; }
        decimal CopayInNetworkPhysicalTherapyVisit { get; set; }
        string PreAuthorizationDepartmentPhoneNumber { get; set; }
        string PreAuthorizationDepartmentFaxNumber { get; set; }
        bool PolicyhasBothInAndOutOfNetworkBenefits { get; set; }
        bool DentalCoverage { get; set; }
        bool OrthoCoverage { get; set; }
        string Remarks { get; set; }
    }

    public interface IRundownCode
    {
        int PatientId { get; set; }
        string PatientInsuranceId { get; set; }
        string CategoryCode { get; set; }
        bool ValidAndBillable { get; set; }
        string ExclusionsOrLimitationsRemarks { get; set; }
        decimal InNetworkCoInsurancePct { get; set; }
        string OutOfNetworkCoInsurancePct { get; set; }
        bool PreauthRequired { get; set; }
        bool HasDollarAmountWhichRequiresPreAuth { get; set; }
        decimal Amount { get; set; }
    }
}
