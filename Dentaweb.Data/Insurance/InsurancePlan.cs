﻿namespace Dentaweb.Data.Insurance
{
    public class InsurancePlan : IInsurancePlan
    {
        public int Id { get; set; }
        public string PlanName { get; set; }
        public int CompanyId { get; set; }
    }
}
