﻿using BaseLibrary.Interfaces;
using System;

namespace Dentaweb.Data.Insurance
{
    public class RundownCode : IRundownCode, IPersistedEntity
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public string PatientInsuranceId { get; set; }
        public string CategoryCode { get; set; }
        public bool ValidAndBillable { get; set; }
        public string ExclusionsOrLimitationsRemarks { get; set; }
        public decimal InNetworkCoInsurancePct { get; set; }
        public string OutOfNetworkCoInsurancePct { get; set; }
        public bool PreauthRequired { get; set; }
        public bool HasDollarAmountWhichRequiresPreAuth { get; set; }
        public decimal Amount { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
