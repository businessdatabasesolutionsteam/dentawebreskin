﻿using System;

namespace Dentaweb.Data.Insurance
{
    public class InsurancePreAuthorization : IInsurancePreAuthorization
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public int OfficeId { get; set; }
        public string InsuranceId { get; set; }
        public DateTime PreAuthDate { get; set; }
        public int AppointmentId { get; set; }
        public int ProviderId { get; set; }
        public int ProductCode { get; set; }
        public PreAuthorizationType PreAuthorizationStatus { get; set; }
        public string PreAuthConfirmationNumber { get; set; }
    }

    public enum PreAuthorizationType
    {
        Approved = 1,
        NeedsResubmission = 2,
        Denied = 3
    }
}
