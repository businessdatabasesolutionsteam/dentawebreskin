﻿using BaseLibrary.Enums;
using BaseLibrary.Interfaces;
using Dentaweb.Data.Enums;
using System;

namespace Dentaweb.Data.Insurance
{
    public class InsurancePolicyBase
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public int InsuranceCompanyId { get; set; }
        public string InsuranceCompanyName { get; set; }
        public InsuranceType InsuranceType { get; set; }
        public InsurancePolicyType PolicyType { get; set; }
    }

    public class InsurancePolicy : InsurancePolicyBase, IInsurancePolicy, IPersistedEntity
    {
        public string EmployerName { get; set; }
        public string EmployerGroupPolicyNumber { get; set; }
        public MedicalType MedicalType { get; set; }
        public string InsurancePlanId { get; set; }
        public InsuranceRelationship RelationshipToPolicyHolder { get; set; }
        public string PolicyHolderLastName { get; set; }
        public string PolicyHolderFirstName { get; set; }
        public string PolicyHolderMiddleName { get; set; }
        public DateTime PolicyHolderDateofBirth { get; set; }
        public Gender PolicyHolderGender { get; set; }
        public string EmployerAddress { get; set; }
        public string EmployerPhone { get; set; }
        public DateTime CoverageEffectiveDate { get; set; }
        public string PolicyNumberMemberID { get; set; }
        public string PolicyHolderAddress1 { get; set; }
        public string PolicyHolderAddress2 { get; set; }
        public string PolicyHolderZipCode { get; set; }
        public string PolicyHolderCity { get; set; }
        public string PolicyHolderState { get; set; }
        public string PolicyHolderCountry { get; set; }
        public string PolicyHolderHomePhone { get; set; }
        public string PolicyHolderWorkPhone { get; set; }
        public string PolicyHolderCellPhone { get; set; }
        public string EmergencyContactPersonName { get; set; }
        public string EmergencyContactRelation { get; set; }
        public string EmergencyContactPhone { get; set; }
        public string PolicyHolderEmail { get; set; }
        public bool PolicyIsActive { get; set; }
        public decimal CoPayment { get; set; }
        public string ThirdPartyNumber { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
