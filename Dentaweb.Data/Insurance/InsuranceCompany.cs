﻿using Dentaweb.Data.Enums;

namespace Dentaweb.Data.Insurance
{
    public class InsuranceCompany : IInsuranceCompany
    {
        public InsuranceType InsuranceType { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Phone { get; set; }
        public string ClaimPhone { get; set; }
        public string PreAuthPhone { get; set; }
        public string AppealPhone { get; set; }
    }
}
