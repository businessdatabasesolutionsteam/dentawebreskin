﻿using BaseLibrary.Enums;
using BaseLibrary.Interfaces;
using System;

namespace Dentaweb.Data.Patients
{
    public class PatientBase
    {
        public int Id { get; set; }
        public virtual string PatientName { get; set; }
        public string Telephone { get; set; }
        public string EmailAddress { get; set; }
        public DateTime DateOfBirth { get; set; }
    }

    public class Patient : PatientBase, IPatient, IPersistedEntity
    {
        public string ServiceType { get; set; }
        public PersonalTitle Title { get; set; }
        public string Salutation { get; set; } // todo Salutation ID ?
        public Gender Gender { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public override string PatientName { get { return LastName + ", " + FirstName; } }
        public MaritalStatus MaritalStatus { get; set; }
        public string DriverLicenseNo { get; set; }
        public string SocialSecurityNo { get; set; }
        public Ethnicity Ethnicity { get; set; }
        public Ethnicity Race { get; set; } // todo
        public ReferralSource ReferralSource { get; set; }
        public int ReferralSourceId { get { return (int)ReferralSource; } }
        public string PreviousDentist { get; set; }
        public string PreviousDentistPhone { get; set; }
        public StudentStatus StudentStatus { get; set; }
        public bool HasDentalInsurance { get; set; }
        public bool HasMedicalInsurance { get; set; }
        public string PreviousProvider { get; set; }
        public string PrevProviderPhone { get; set; }
        public string MobilePhone { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string EmergencyContact { get; set; }
        public string RelationshipToPatient { get; set; }
        public string EmergencyContactPhone { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
