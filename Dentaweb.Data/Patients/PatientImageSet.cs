﻿using Dentaweb.Data.Enums;
using Dentaweb.Data.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Patients
{
    // todo do these always get uploaded as a set, or can they be changed individually?
    public abstract class ImageSet
    {
        public int PatiendId { get; set; }
        public int PatientImageSetId { get; set; }
        public string CaseId { get; set; }

        protected string GetImagePath(PatientImageType imagetype)
        {
            return ImageHelper.GetImagePathForPatientImage(PatiendId, PatientImageSetId, imagetype);
        }
    }

    public class PatientImageSet : ImageSet
    {
        public string FrontRelaxedImagePath { get { return GetImagePath(PatientImageType.FrontRelaxed); } }
        public string ProfileImagePath { get { return GetImagePath(PatientImageType.Profile); } }
        public string SubmentoVertexImagePath { get { return GetImagePath(PatientImageType.SubmentoVertex); } }
        public string FrontFullBodyPostureImagePath { get { return GetImagePath(PatientImageType.FrontFullBodyPosture); } }
        public string PosteriorFullBodyPostureImagePath { get { return GetImagePath(PatientImageType.PosteriorFullBodyPosture); } }
    }

    public class IntraOralImageSet : ImageSet
    {
        public string AnteriorImagePath { get { return GetImagePath(PatientImageType.Anterior); } }
        public string LeftBuccalImagePath { get { return GetImagePath(PatientImageType.LeftBuccal); } }
        public string RightBuccalImagePath { get { return GetImagePath(PatientImageType.RightBuccal); } }
        public string UpperArchImagePath { get { return GetImagePath(PatientImageType.UpperArch); } }
        public string LowerArchImagePath { get { return GetImagePath(PatientImageType.LowerArch); } }
        public string LingualFrenumImagePath { get { return GetImagePath(PatientImageType.LingualFrenum); } }
    }

    public class CbctImageSet : ImageSet
    {
        public string CBCTScanImagePath { get { return GetImagePath(PatientImageType.CBCTScan); } }
        public string LateralCephImagePath { get { return GetImagePath(PatientImageType.LateralCeph); } }
        public string PanoImagePath { get { return GetImagePath(PatientImageType.Pano); } }
        public string CephImagePath { get { return GetImagePath(PatientImageType.Ceph); } }
        public string TomographLeftImagePath { get { return GetImagePath(PatientImageType.TomographLeft); } }
        public string TomographRightImagePath { get { return GetImagePath(PatientImageType.TomographRight); } }
    }

    public class Occlusion3dImageSet : ImageSet
    {
        public string UpperArchImagePath { get { return GetImagePath(PatientImageType.OcclusionUpperArch); } }
        public string LowerArchImagePath { get { return GetImagePath(PatientImageType.OcclusionLowerArch); } }
        public string NaturalBiteImagePath { get { return GetImagePath(PatientImageType.OcclusionNaturalBite); } }
        public string ConstructedBiteImagePath { get { return GetImagePath(PatientImageType.OcclusionConstructedBite); } }
    }

    public class Occlusion2dImageSet : ImageSet
    {
        public string UpperArchImagePath { get { return GetImagePath(PatientImageType.OcclusionUpperArch); } }
        public string LowerArchImagePath { get { return GetImagePath(PatientImageType.OcclusionLowerArch); } }
        public string FrontImagePath { get { return GetImagePath(PatientImageType.OcclusionFront); } }
        public string LeftImagePath { get { return GetImagePath(PatientImageType.OcclusionLeft); } }
        public string RightImagePath { get { return GetImagePath(PatientImageType.OcclusionRight); } }
        public OcclusionRelationship RightMolarRelationship { get; set; }
        public OcclusionRelationship LeftMolarRelationship { get; set; }
        public string ImpressionMadeBy { get; set; }
    }
}
