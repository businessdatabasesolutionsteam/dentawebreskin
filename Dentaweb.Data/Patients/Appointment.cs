﻿using BaseLibrary.Interfaces;
using System;

namespace Dentaweb.Data.Patients
{
    public class Appointment : IAppointment, IPersistedEntity
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public int OfficeId { get; set; }
        public DateTime AppointmentDateTime { get; set; }
        public string AppointmentStatus { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
