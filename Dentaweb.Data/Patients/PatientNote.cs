﻿using BaseLibrary.Interfaces;
using System;

namespace Dentaweb.Data.Patients
{
    public class PatientNote : IPatientNote, IPersistedEntity
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public PatientNoteType NoteType { get; set; }
        public string NoteText { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }

    public enum PatientNoteType
    {
        General,
        Clinical,
        MedicalAlerts
    }
}

