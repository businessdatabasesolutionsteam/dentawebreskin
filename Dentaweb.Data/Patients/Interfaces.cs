﻿using BaseLibrary.Enums;
using System;

namespace Dentaweb.Data.Patients
{
    // todo figure out interfaces. Currently these should define enough information to save new 
    // entities. The idea is to map to Sql Server UDTs to simplify making database calls

    public interface IPatient
    {
        string PatientName { get; set; }
        string Telephone { get; set; }
        string EmailAddress { get; set; }
        DateTime DateOfBirth { get; set; }

        string ServiceType { get; set; }
        PersonalTitle Title { get; set; }
        string Salutation { get; set; }
        Gender Gender { get; set; }
        string LastName { get; set; }
        string FirstName { get; set; }
        MaritalStatus MaritalStatus { get; set; }
        string DriverLicenseNo { get; set; }
        string SocialSecurityNo { get; set; }
        Ethnicity Ethnicity { get; set; }
        Ethnicity Race { get; set; }
        ReferralSource ReferralSource { get; set; }
        string PreviousDentist { get; set; }
        string PreviousDentistPhone { get; set; }
        StudentStatus StudentStatus { get; set; }
        bool HasDentalInsurance { get; set; }
        bool HasMedicalInsurance { get; set; }
        string PreviousProvider { get; set; }
        string PrevProviderPhone { get; set; }
        string MobilePhone { get; set; }
        string HomePhone { get; set; }
        string WorkPhone { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Zip { get; set; }
        string Country { get; set; }
        string EmergencyContact { get; set; }
        string RelationshipToPatient { get; set; }
        string EmergencyContactPhone { get; set; }
        string CreatedBy { get; set; }
        DateTime CreatedOn { get; set; }
        string UpdatedBy { get; set; }
        DateTime? UpdatedOn { get; set; }
    }

    public interface IPatientNote
    {
        int PatientId { get; set; }
        PatientNoteType NoteType { get; set; }
        string NoteText { get; set; }
    }

    public interface IAppointment
    {
        int PatientId { get; set; }
        int OfficeId { get; set; }
        DateTime AppointmentDateTime { get; set; }
        string AppointmentStatus { get; set; }
    }
}
