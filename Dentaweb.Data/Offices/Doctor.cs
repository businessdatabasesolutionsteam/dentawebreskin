﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Offices
{
    public class Doctor
    {
        public int Id { get; set; }
        public string DoctorName { get; set; }
    }
}
