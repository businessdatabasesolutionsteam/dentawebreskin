﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Offices
{
    public class Office : IOffice
    {
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string Specialty { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Phone { get; set; }
        public string CorporationName { get; set; }
        public string TaxId { get; set; }
        public string NPIType { get; set; }
        public string NPINumber { get; set; }
        public DateTime NPIEffectiveDate { get; set; }
        public string Taxonomy { get; set; }
    }
}
