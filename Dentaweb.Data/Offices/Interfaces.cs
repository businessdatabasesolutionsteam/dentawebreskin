﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Offices
{
    public interface IOffice
    {
        string OfficeName { get; set; }
        string Specialty { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Phone { get; set; }
        string CorporationName { get; set; }
        //string TaxIdName { get; set; } ???
        string TaxId { get; set; }
        string NPIType { get; set; } // todo what is this?
        string NPINumber { get; set; } // todo ?
        DateTime NPIEffectiveDate { get; set; }
        string Taxonomy { get; set; }
    }
}
