﻿using Dentaweb.Data.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Dentaweb.Data.Helpers
{
    public static class ImageHelper
    {
        private static readonly string imagepath = "~/path"; // todo where will images be stored?

        public static string GetImagePathForPatientImage(int patientid, int imagesetid, PatientImageType imagetype)
        {
            // todo which image types are supported (png, etc.)?
            string filename = $"{patientid}_{imagesetid}_{imagetype}.jpg";
            return String.Join("/", imagepath, patientid.ToString(), imagesetid.ToString(), filename);
        }
    }
}
