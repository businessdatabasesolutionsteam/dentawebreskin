﻿using Dentaweb.Data.Submissions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Repositories.Interfaces
{
    public interface ISubmissionRepository
    {
        IEnumerable<SubmissionHealthHistory> GetEntireHealthHistory(int patientid);
        //SubmissionHealthHistory GetHealthHistoryRecord(int historyid);
        IEnumerable<SubmissionToothCharting> GetPatientToothCharting(int patientid);

        IEnumerable<SubmissionArchAnalysis> GetPatientArchAnalysis(int patientid);
        IEnumerable<SubmissionPatientObjectives> GetPatientObjectives(int patientid);
        IEnumerable<SubmissionMiscDocs> GetPatientMiscDocs(int patientid);
        IEnumerable<SubmissionOrderDetails> GetPatientOrderDetails(int patientid);        
    }
}
