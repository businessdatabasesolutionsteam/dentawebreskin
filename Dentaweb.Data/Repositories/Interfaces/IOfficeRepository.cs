﻿using Dentaweb.Data.Offices;
using Dentaweb.Data.Patients;
using System.Collections.Generic;

namespace Dentaweb.Data.Repositories.Interfaces
{
    public interface IOfficeRepository
    {
        IEnumerable<Doctor> GetDoctors(int officeId);

        Office GetOffice(int officeId);
    }
}
