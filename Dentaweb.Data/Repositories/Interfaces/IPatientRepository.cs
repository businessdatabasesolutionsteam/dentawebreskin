﻿using Dentaweb.Data.Patients;
using System.Collections.Generic;

namespace Dentaweb.Data.Repositories.Interfaces
{
    public interface IPatientRepository
    {
        IEnumerable<PatientBase> GetPatients(string search);

        Patient GetPatient(int patientid);

        int Create(IPatient patient);

        bool Update(int patientid, IPatient patient);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="patientid"></param>
        /// <returns>true if successful, otherwise false</returns>
        bool Delete(int patientid);

        #region Notes
        IEnumerable<PatientNote> GetNotes(int patientid);

        PatientNote GetNote(int noteid);

        int CreateNote(IPatientNote note);

        bool UpdateNote(int noteid, IPatientNote note);

        bool DeleteNote(int noteid);
        #endregion

        #region Appointments
        IEnumerable<Appointment> GetAppointments(int patientid); //officeid?

        Appointment GetAppointment(int appointmentid);

        int CreateAppointment(IAppointment appointment);

        bool UpdateAppointment(int appointmentid, IAppointment appointment);

        bool DeleteAppointment(int appointmentid);
        #endregion
    }
}
