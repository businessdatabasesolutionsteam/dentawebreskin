﻿using Dentaweb.Data.Insurance;
using System.Collections.Generic;

namespace Dentaweb.Data.Repositories.Interfaces
{
    public interface IInsuranceRepository
    {
        #region Policy
        IEnumerable<InsurancePolicyBase> GetInsurancePolicies(int patientid);

        InsurancePolicy GetInsurancePolicy(int policyid);

        int CreateInsurancePolicy(IInsurancePolicy policy);

        bool UpdateInsurancePolicy(int policyid, IInsurancePolicy policy);

        bool DeleteInsurancePolicy(int policyid);
        #endregion

        #region PreAuth
        IEnumerable<InsurancePreAuthorization> GetInsurancePreAuthorizations(int patientid); //officeid?

        InsurancePreAuthorization GetInsurancePreAuthorization(int preauthid);

        int CreateInsurancePreAuthorization(IInsurancePreAuthorization preauth);

        bool UpdateInsurancePreAuthorization(int preauthid, IInsurancePreAuthorization preauth);

        bool DeleteInsurancePreAuthorization(int preauthid);
        #endregion

        #region Benefits
        Benefits GetBenefitsRundown(int benefitsid);
        IEnumerable<Benefits> GetBenefitsRundowns(int patientid);
        int CreateBenefits(IBenefits benefits);
        bool UpdateBenefits(int benefitsid, IBenefits benefits);
        bool DeleteBenefits(int benefitsid);
        #endregion

        #region RundownCodes
        RundownCode GetRundownCode(int rundownid);
        IEnumerable<RundownCode> GetRundownCodes(int patientid);
        int CreateRundownCode(IRundownCode rundowncode);
        bool UpdateRundownCode(int rundowncodeid, IRundownCode rundowncode);
        bool DeleteRundownCode(int rundowncodeid);
        #endregion

        IEnumerable<InsuranceCompany> GetInsuranceCompanies();

        IEnumerable<InsurancePlan> GetInsurancePlans();
    }
}
