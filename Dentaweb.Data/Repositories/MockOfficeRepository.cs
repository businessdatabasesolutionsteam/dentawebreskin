﻿using System;
using System.Collections.Generic;
using BaseLibrary.Exceptions;
using Dentaweb.Data.Offices;
using Dentaweb.Data.Repositories.Interfaces;

namespace Dentaweb.Data.Repositories
{
    public class MockOfficeRepository : IOfficeRepository
    {
        public IEnumerable<Doctor> GetDoctors(int officeId)
        {
            if (officeId > 5000)
            {
                throw new EntityNotFoundException($"No office exists with the id '{officeId}'", officeId);
            }

            yield return new Doctor()
            {
                Id = 1,
                DoctorName = "Hibbert, Julius M."
            };

            yield return new Doctor()
            {
                Id = 2,
                DoctorName = "Riviera, Nick"
            };
        }

        public Office GetOffice(int officeId)
        {
            if (officeId > 5000)
            {
                throw new EntityNotFoundException($"No office exists with the id '{officeId}'", officeId);
            }

            return new Office()
            {
                OfficeId = officeId,
                OfficeName = "Springfield General",
                Specialty = "General",
                Address1 = "1234 Main St",
                Address2 = "",
                City = "Springfield",
                State = "",
                Phone = "(555) 555-1234",
                CorporationName = "Springfield, LLC.",
                TaxId = "55-222-3333",
                NPIType = "",
                NPINumber = "",
                NPIEffectiveDate = new DateTime(2010, 6, 1),
                Taxonomy = "",
            };
        }
    }
}
