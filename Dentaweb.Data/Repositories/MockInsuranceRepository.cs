﻿using BaseLibrary.Exceptions;
using Dentaweb.Data.Enums;
using Dentaweb.Data.Insurance;
using Dentaweb.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dentaweb.Data.Repositories
{
    public class MockInsuranceRepository : IInsuranceRepository
    {
        #region Policy
        public IEnumerable<InsurancePolicyBase> GetInsurancePolicies(int patientid)
        {
            yield return new InsurancePolicyBase()
            {
                Id = 32760,
                PatientId = patientid,
                InsuranceCompanyId = 1,
                InsuranceCompanyName = "Acme Insurance",
                InsuranceType = InsuranceType.HMO,
                PolicyType = InsurancePolicyType.Primary,
            };

            yield return new InsurancePolicyBase()
            {
                Id = 30200,
                PatientId = patientid,
                InsuranceCompanyId = 2,
                InsuranceCompanyName = "Allied Insurance",
                InsuranceType = InsuranceType.PPO,
                PolicyType = InsurancePolicyType.Secondary,
            };
        }

        public InsurancePolicy GetInsurancePolicy(int policyid)
        {
            var policy = GetInsurancePolicies(1).First();
            policy.Id = policyid;
            return new InsurancePolicy()
            {
                Id = policy.Id,
                PatientId = policy.PatientId,
                InsuranceCompanyId = policy.InsuranceCompanyId,
                InsuranceCompanyName = policy.InsuranceCompanyName,
                InsuranceType = policy.InsuranceType,
                PolicyType = policy.PolicyType,
                // todo mock more data
            };
        }

        public int CreateInsurancePolicy(IInsurancePolicy policy)
        {
            if (policy.PatientId > 5000)
            {
                throw new EntityNotFoundException($"No patient exists with the id '{policy.PatientId}'", policy.PatientId);
            }

            return 3316;
        }

        public bool UpdateInsurancePolicy(int policyid, IInsurancePolicy policy)
        {
            return policyid < 5000 ? true : false;
        }

        public bool DeleteInsurancePolicy(int policyid)
        {
            return policyid < 5000 ? true : false;
        }
        #endregion

        #region InsurancePreAuth
        public IEnumerable<InsurancePreAuthorization> GetInsurancePreAuthorizations(int patientid)
        {
            yield return new InsurancePreAuthorization()
            {
                Id = 54,
                PatientId = patientid,
                OfficeId = 1,
                InsuranceId = "x12345",
                PreAuthDate = DateTime.Today.AddDays(-30),
                AppointmentId = 276,
                ProviderId = 2,
                ProductCode = 546,
                PreAuthorizationStatus = PreAuthorizationType.Approved,
                PreAuthConfirmationNumber = "xyz987654",
            };

            yield return new InsurancePreAuthorization()
            {
                Id = 58,
                PatientId = patientid,
                OfficeId = 1,
                InsuranceId = "abc123",
                PreAuthDate = DateTime.Today.AddDays(-60),
                AppointmentId = 276,
                ProviderId = 2,
                ProductCode = 541,
                PreAuthorizationStatus = PreAuthorizationType.Denied,
                PreAuthConfirmationNumber = "",
            };

            yield return new InsurancePreAuthorization()
            {
                Id = 58,
                PatientId = patientid,
                OfficeId = 1,
                InsuranceId = "x12345",
                PreAuthDate = DateTime.Today.AddDays(-15),
                AppointmentId = 276,
                ProviderId = 2,
                ProductCode = 546,
                PreAuthorizationStatus = PreAuthorizationType.NeedsResubmission,
                PreAuthConfirmationNumber = "",
            };
        }

        public InsurancePreAuthorization GetInsurancePreAuthorization(int preauthid)
        {
            var preauth = GetInsurancePreAuthorizations(1).First();
            preauth.Id = preauthid;
            return preauth;
        }

        public int CreateInsurancePreAuthorization(IInsurancePreAuthorization preauth)
        {
            if (preauth.PatientId > 5000)
            {
                throw new EntityNotFoundException($"No patient exists with the id '{preauth.PatientId}'", preauth.PatientId);
            }

            return 276;
        }

        public bool UpdateInsurancePreAuthorization(int preauthid, IInsurancePreAuthorization preauth)
        {
            return preauthid < 5000 ? true : false;
        }

        public bool DeleteInsurancePreAuthorization(int preauthid)
        {
            return preauthid < 5000 ? true : false;
        }
        #endregion

        #region Benefits
        public IEnumerable<Benefits> GetBenefitsRundowns(int patientid)
        {
            yield return new Benefits()
            {
                Id = 6543,
                PatientId = patientid,
                PatientInsuranceId = "x432",
                InsuranceCompanyId = 2,
                RepresentativeName = "Bob Jones",
                RepresentativeId = "TH352",
                CoverageEffectiveDate = new DateTime(2018, 3, 1),
                BenefitYear = 2018,
                DeductibleInNetworkIndividual = 500m,
                DeductibleInNetworkFamily = 1000m,
                InNetworkDeductibleMetIndividual = false,
                InNetworkDeductibleMetFamily = false,
                InNetworkOutOfPocketMaxIndividual = 10000m,
                InNetworkOutOfPocketMaxFamily = 20000m,
                InNetworkOutOfPocketMetIndividual = false,
                InNetworkOutOfPocketMetFamily = false,
                OutOfNetworkDeductibleIndividual = 10000m,
                OutOfNetworkDeductibleFamily = 20000m,
                OutOfNetworkDeductibleMetIndividual = false,
                OutOfNetworkDeductibleMetFamily = false,
                OutOfNetworkOutOfPocketMaxIndividual = 25000m,
                OutOfNetworkOutOfPocketMaxFamily = 30000m,
                OutOfNetworkOutOfPocketMetIndividual = false,
                OutOfNetworkOutOfPocketMetFamily = false,
                CopayInNetworkSpecialistVisit = 80m,
                CopayInNetworkPhysicalTherapyVisit = 90m,
                PreAuthorizationDepartmentPhoneNumber = "(323) 555-7777",
                PreAuthorizationDepartmentFaxNumber = "(323) 555-7778",
                PolicyhasBothInAndOutOfNetworkBenefits = true,
                DentalCoverage = true,
                OrthoCoverage = false,
                Remarks = "No remarks",
                CreatedBy = "bjones@example.com",
                CreatedOn = new DateTime(2018, 2, 1),
                UpdatedBy = "",
                UpdatedOn = null,
            };
        }

        public Benefits GetBenefitsRundown(int benefitsid)
        {
            var benefits = GetBenefitsRundowns(1).First();
            benefits.Id = benefitsid;
            return benefits;
        }

        public int CreateBenefits(IBenefits benefits)
        {
            if (benefits.PatientId > 5000)
            {
                throw new EntityNotFoundException($"No patient exists with the id '{benefits.PatientId}'", benefits.PatientId);
            }

            return 214;
        }

        public bool UpdateBenefits(int benefitsid, IBenefits benefits)
        {
            return benefitsid < 5000 ? true : false;
        }

        public bool DeleteBenefits(int benefitsid)
        {
            return benefitsid < 5000 ? true : false;
        }
        #endregion

        #region RundownCodes
        public IEnumerable<RundownCode> GetRundownCodes(int patientid)
        {
            yield return new RundownCode()
            {
                Id = 874,
                PatientId = patientid,
                PatientInsuranceId = "XYZ123",
                CategoryCode = "",
                ValidAndBillable = true,
                ExclusionsOrLimitationsRemarks = "",
                InNetworkCoInsurancePct = 20m,
                OutOfNetworkCoInsurancePct = "",
                PreauthRequired = true,
                HasDollarAmountWhichRequiresPreAuth = true,
                Amount = 50000m,
                CreatedBy = "bjones@example.com",
                CreatedOn = new DateTime(2010, 5, 6),
            };
        }

        public RundownCode GetRundownCode(int rundownid)
        {
            var rundown = GetRundownCodes(1).First();
            rundown.Id = rundownid;
            return rundown;
        }

        public int CreateRundownCode(IRundownCode rundowncode)
        {
            if (rundowncode.PatientId > 5000)
            {
                throw new EntityNotFoundException($"No patient exists with the id '{rundowncode.PatientId}'", rundowncode.PatientId);
            }

            return 214;
        }

        public bool UpdateRundownCode(int rundowncodeid, IRundownCode rundowncode)
        {
            return rundowncodeid < 5000 ? true : false;
        }

        public bool DeleteRundownCode(int rundowncodeid)
        {
            return rundowncodeid < 5000 ? true : false;
        }
        #endregion

        public IEnumerable<InsuranceCompany> GetInsuranceCompanies()
        {
            yield return new InsuranceCompany()
            {
                InsuranceType = InsuranceType.PPO,
                CompanyId = 2,
                Name = "Acme Insurance",
                Address = "123 Main St",
                City = "Los Angeles",
                State = "CA",
                Phone = "(323) 555-1234",
                ClaimPhone = "(323) 555-1235",
                PreAuthPhone = "(323) 555-1236",
                AppealPhone = "(323) 555-1237",
            };

            yield return new InsuranceCompany()
            {
                InsuranceType = InsuranceType.HMO,
                CompanyId = 3,
                Name = "Allied Insurance",
                Address = "1234 Broad Blvd",
                City = "Los Angeles",
                State = "CA",
                Phone = "(323) 555-1234",
                ClaimPhone = "(323) 555-1235",
                PreAuthPhone = "(323) 555-1236",
                AppealPhone = "(323) 555-1237",
            };
        }

        public IEnumerable<InsurancePlan> GetInsurancePlans()
        {
            yield return new InsurancePlan()
            {
                Id = 82,
                PlanName = "Bronze Plan",
                CompanyId = 2
            };

            yield return new InsurancePlan()
            {
                Id = 4,
                PlanName = "Silver Plan",
                CompanyId = 2
            };

            yield return new InsurancePlan()
            {
                Id = 5,
                PlanName = "Gold Plan",
                CompanyId = 2
            };

            yield return new InsurancePlan()
            {
                Id = 88,
                PlanName = "Market Plan",
                CompanyId = 3
            };
        }
    }
}
