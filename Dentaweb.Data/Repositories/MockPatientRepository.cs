﻿using BaseLibrary.Enums;
using BaseLibrary.Exceptions;
using Dentaweb.Data.Insurance;
using Dentaweb.Data.Patients;
using Dentaweb.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dentaweb.Data.Repositories
{
    public class MockPatientRepository : IPatientRepository
    {
        public IEnumerable<PatientBase> GetPatients(string search)
        {
            yield return new PatientBase()
            {
                Id = 1,
                PatientName = "Simpson, Homer",
                Telephone = "(323) 555-1234",
                EmailAddress = "homer@springfieldnuclear.com",
                DateOfBirth = new DateTime(1956, 5, 12),
            };

            yield return new PatientBase()
            {
                Id = 2,
                PatientName = "Jones, Tom",
                Telephone = "(212) 555-4321",
                EmailAddress = "tom@jones.com",
                DateOfBirth = new DateTime(1940, 6, 7),
            };
        }

        public Patient GetPatient(int patientid)
        {
            var patient = GetPatients("").First();

            return new Patient()
            {
                Id = patientid,
                PatientName = patient.PatientName,
                Telephone = patient.Telephone,
                EmailAddress = patient.EmailAddress,
                ServiceType = "",
                Title = PersonalTitle.Mr,
                Salutation = "",
                Gender = Gender.Male,
                LastName = "Simpson",
                FirstName = "Homer",
                MaritalStatus = MaritalStatus.Married,
                DriverLicenseNo = "",
                SocialSecurityNo = "",
                Ethnicity = Ethnicity.Caucasian,
                Race = Ethnicity.Caucasian,
                ReferralSource = ReferralSource.WalkIn,
                PreviousDentist = "",
                PreviousDentistPhone = "",
                StudentStatus = StudentStatus.SomeCollege,
                HasDentalInsurance = true,
                HasMedicalInsurance = true,
                PreviousProvider = "",
                PrevProviderPhone = "",
                MobilePhone = "",
                HomePhone = "",
                WorkPhone = "",
                Address1 = "",
                Address2 = "",
                City = "",
                State = "",
                Zip = "",
                Country = "",
                EmergencyContact = "",
                RelationshipToPatient = "",
                EmergencyContactPhone = "",
                CreatedBy = "bjohnson@example.com",
                CreatedOn = new DateTime(2018, 11, 5),
                UpdatedBy = "",
                UpdatedOn = null
            };
        }

        /// <summary>
        ///     Create a patient and return the new id
        /// </summary>
        /// <param name="patient"></param>
        /// <returns></returns>
        public int Create(IPatient patient)
        {
            return 42;
        }

        public bool Update(int patientid, IPatient patient)
        {
            return patientid < 5000 ? true : false;
        }

        public bool Delete(int patientid)
        {
            return patientid < 5000 ? true : false;
        }

        #region Notes
        public IEnumerable<PatientNote> GetNotes(int patientid)
        {
            yield return new PatientNote()
            {
                PatientId = patientid,
                Id = 24,
                NoteType = PatientNoteType.General,
                NoteText = "Patient is happy to be here.",
                CreatedBy = "bsmith@example.com",
                CreatedOn = new DateTime(2018, 12, 3),
                UpdatedBy = "",
                UpdatedOn = null
            };

            yield return new PatientNote()
            {
                PatientId = patientid,
                Id = 28,
                NoteType = PatientNoteType.Clinical,
                NoteText = "Has sleep apnea",
                CreatedBy = "bsmith@example.com",
                CreatedOn = new DateTime(2018, 12, 6),
                UpdatedBy = "jdoe@example.com",
                UpdatedOn = new DateTime(2018, 12, 8)
            };
        }

        public PatientNote GetNote(int noteid)
        {
            var note = GetNotes(1).First();
            note.Id = noteid;
            return note;
        }

        public int CreateNote(IPatientNote note)
        {
            if (note.PatientId > 5000)
            {
                throw new EntityNotFoundException($"No patient exists with the id '{note.PatientId}'", note.PatientId);
            }

            return 324;
        }

        public bool UpdateNote(int noteid, IPatientNote note)
        {
            return noteid < 5000 ? true : false;
        }

        public bool DeleteNote(int noteid)
        {
            return noteid < 5000 ? true : false;
        }

        #endregion

        #region Appointments
        public IEnumerable<Appointment> GetAppointments(int patientid)
        {
            yield return new Appointment()
            {
                PatientId = patientid,
                OfficeId = 4,
                AppointmentDateTime = new DateTime(2019, 4, 3),
                AppointmentStatus = "Scheduled",
            };

            yield return new Appointment()
            {
                PatientId = patientid,
                OfficeId = 4,
                AppointmentDateTime = new DateTime(2019, 8, 2),
                AppointmentStatus = "Scheduled",
            };

            yield return new Appointment()
            {
                PatientId = patientid,
                OfficeId = 4,
                AppointmentDateTime = new DateTime(2019, 4, 3),
                AppointmentStatus = "Canceled",
            };
        }

        public Appointment GetAppointment(int appointmentid)
        {
            var appointment = GetAppointments(1).First();
            appointment.Id = appointmentid;
            return appointment;
        }

        public int CreateAppointment(IAppointment appointment)
        {
            if (appointment.PatientId > 5000)
            {
                throw new EntityNotFoundException($"No patient exists with the id '{appointment.PatientId}'", appointment.PatientId);
            }

            return 276;
        }

        public bool UpdateAppointment(int appointmentid, IAppointment appointment)
        {
            return appointmentid < 5000 ? true : false;
        }

        public bool DeleteAppointment(int appointmentid)
        {
            return appointmentid < 5000 ? true : false;
        }
        #endregion

        #region InsurancePreAuth
        public IEnumerable<InsurancePreAuthorization> GetInsurancePreAuthorizations(int patientid)
        {
            yield return new InsurancePreAuthorization()
            {
                Id = 54,
                PatientId = patientid,
                OfficeId = 1,
                InsuranceId = "x12345",
                PreAuthDate = DateTime.Today.AddDays(-30),
                AppointmentId = 276,
                ProviderId = 2,
                ProductCode = 546,
                PreAuthorizationStatus = PreAuthorizationType.Approved,
                PreAuthConfirmationNumber = "xyz987654",
            };

            yield return new InsurancePreAuthorization()
            {
                Id = 58,
                PatientId = patientid,
                OfficeId = 1,
                InsuranceId = "abc123",
                PreAuthDate = DateTime.Today.AddDays(-60),
                AppointmentId = 276,
                ProviderId = 2,
                ProductCode = 541,
                PreAuthorizationStatus = PreAuthorizationType.Denied,
                PreAuthConfirmationNumber = "",
            };

            yield return new InsurancePreAuthorization()
            {
                Id = 58,
                PatientId = patientid,
                OfficeId = 1,
                InsuranceId = "x12345",
                PreAuthDate = DateTime.Today.AddDays(-15),
                AppointmentId = 276,
                ProviderId = 2,
                ProductCode = 546,
                PreAuthorizationStatus = PreAuthorizationType.NeedsResubmission,
                PreAuthConfirmationNumber = "",
            };
        }

        public InsurancePreAuthorization GetInsurancePreAuthorization(int preauthid)
        {
            var preauth = GetInsurancePreAuthorizations(1).First();
            preauth.Id = preauthid;
            return preauth;
        }

        public int CreateInsurancePreAuthorization(IInsurancePreAuthorization preauth)
        {
            if (preauth.PatientId > 5000)
            {
                throw new EntityNotFoundException($"No patient exists with the id '{preauth.PatientId}'", preauth.PatientId);
            }

            return 276;
        }

        public bool UpdateInsurancePreAuthorization(int preauthid, IInsurancePreAuthorization preauth)
        {
            return preauthid < 5000 ? true : false;
        }

        public bool DeleteInsurancePreAuthorization(int preauthid)
        {
            return preauthid < 5000 ? true : false;
        }
        #endregion
    }
}
