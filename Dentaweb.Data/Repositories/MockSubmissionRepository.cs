﻿using Dentaweb.Data.Enums;
using Dentaweb.Data.Repositories.Interfaces;
using Dentaweb.Data.Submissions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dentaweb.Data.Repositories
{
    public class MockSubmissionRepository : ISubmissionRepository
    {
        public IEnumerable<SubmissionHealthHistory> GetEntireHealthHistory(int patientid)
        {
            yield return new SubmissionHealthHistory()
            {
                Id = 35243, 
                PatientId = patientid
            };
        }

        public IEnumerable<SubmissionToothCharting> GetPatientToothCharting(int patientid)
        {
            yield return new SubmissionToothCharting()
            {
                Id = 3343,
                PatientId = patientid
            };
        }

        public IEnumerable<SubmissionArchAnalysis> GetPatientArchAnalysis(int patientid)
        {
            yield return new SubmissionArchAnalysis()
            {
                Id = 1433,
                PatientId = patientid
            };
        }

        public IEnumerable<SubmissionPatientObjectives> GetPatientObjectives(int patientid)
        {
            yield return new SubmissionPatientObjectives()
            {
                Id = 1248,
                PatientId = patientid,
                Appliances = new List<SubmissionPatientObjectivesAppliance>()
                {
                    new SubmissionPatientObjectivesAppliance()
                    {
                        EstTreatTimeAppliance = TreatmentSystem.VivosGuideSystem,
                        EstTreatTimeApproxNumMonthsInUse = 2,
                        EstTreatTimeApproxPatientWearTimeHrsDay = "16:30",
                        EstTreatTimeApproximateNumMonthsOfPassiveUse = 4
                    }
                }
            };
        }

        public IEnumerable<SubmissionMiscDocs> GetPatientMiscDocs(int patientid)
        {
            yield return new SubmissionMiscDocs()
            {
                Id = 8453,
                PatientId = patientid
            };
        }

        public IEnumerable<SubmissionOrderDetails> GetPatientOrderDetails(int patientid)
        {
            yield return new SubmissionOrderDetails()
            {
                Id = 86543,
                PatientId = patientid
            };
        }
    }
}
